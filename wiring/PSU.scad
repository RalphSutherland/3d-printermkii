use <std/boxes.scad>;
use <std/fasteners.scad>;

_width        = 114;//mm, x outside of psu, inside of cap
_height       =  49;// z outside of psu, inside of cap
_length       = 215;// y outside of psu
_depth        =  75;// y inside of cap with connectors, incl sleave
_sleave_depth =  40;// length covering metal in each cap;
_inset_depth  =  20;// difference top and bottom edges
_fardepth     =  15;// y width far end

_fan_diameter = 60;
_fan_insetx   = 47;
_fan_insety   = 47;

_wall         = 3;//mm

_tol          = 0.2;//mm

_screwy = 32.5;//mm 
_screwz = 12.5;//mm 
_screwM = 4;// M3 M4 etc

$fn = 30;

print();

//far_end();
//power_end();

module assembled(){
translate([0,175,0])power_end();

color("silver"){
    translate([_wall+_tol,_tol,_wall+_tol])
    cube([_width,_length,_height-_tol]);
}
translate([_width+2*(_wall+_tol),_screwy,0])
rotate([0,0,180])far_end();

}

module print(){
rotate([-90,0,0]){
   translate([0,   -_depth-_wall, 0])  power_end();
   translate([0,-_fardepth-_wall,75])  far_end();
}

}
 
module power_end(){
    
    difference(){
    union(){
    
     //   main hull
    difference(){
        union(){
        translate([( _width+2*_wall)/2,
                   ( _depth+2*_wall)/2-_wall,
                   (_height+_wall)/2])
        roundedBox([ _width+2*_wall,
                     _depth+2*_wall,
                    _height+_wall],4,true);
        
        translate([( _width+2*_wall)/2,
                   ( _depth/4+2*_wall)/2-_wall,
                   (_height+_wall)/2])
        rotate([90,0,0])
        roundedBox([ _width+2*_wall,
                    _height+_wall,
                   _depth/4+2*_wall],4);
        }//u
        
        translate([( _width+2*_tol)/2+_wall-_tol,
                   ( _depth+_wall+2*_tol)/2-_wall-_tol,
                   (_height+2*_tol)/2+_wall-_tol,])
        cube([ _width+2*_tol,
               _depth+_wall+2*_tol,
               _height+2*_tol],true);
        
        
    }//diff
    
        translate([_wall-_tol,_sleave_depth+_tol,_wall-_tol])
        cube([_width+2*_tol,
              _depth-_sleave_depth+_tol,
              _wall+2*_tol]);
    
        translate([_wall+1.5,_sleave_depth-_inset_depth+0.1,_height])
        cube([_width-3,
              _depth-_inset_depth+_tol,
              _wall]);
    
        translate([_wall-0.5,_sleave_depth+0.5,_height])
        cube([_width+1,
              _depth-_sleave_depth+_tol,
              _wall]);

       hull(){
       translate([(_width+2*(_wall+_tol))-20,_sleave_depth-_screwy, -4])
            cylinder(d1=10,d2=20,h=5);
       translate([20,_sleave_depth-_screwy, -4])
            cylinder(d1=10,d2=20,h=5);
        }

       hull(){
       translate([(_width+2*(_wall+_tol))-20,_depth-10, -4])
            cylinder(d1=10,d2=20,h=5);
       translate([20,_depth-10, -4])
            cylinder(d1=10,d2=20,h=5);
        }


    }//u
 
    // top gap
         //  cube([]);
    
     //   screw holes

      translate([(_width+2*(_wall+_tol))-32-_wall,_sleave_depth-_screwy, -4.1]){
            boltHole(size=_screwM,length=15,tolerance=0.1);
            washerHole(size=_screwM,length=4,tolerance=0.1);
       }
       translate([32+_wall,_sleave_depth-_screwy, -4.1]){
            boltHole(size=_screwM,length=15,tolerance=0.1);
            washerHole(size=_screwM,length=4,tolerance=0.1);
       }
       
        translate([-1,_sleave_depth-_screwy,_screwz+_wall+_tol])
        rotate([0,90,0]) 
            boltHole(size=_screwM,length=15,tolerance=0.1);
        
        translate([_width-1,_sleave_depth-_screwy,_screwz+_wall+_tol])
        rotate([0,90,0]) 
            boltHole(size=_screwM,length=15,tolerance=0.1);

      // lead holes and power switch
       
       translate([24+_wall*3,_depth-14+_wall+2.5,_height/2+_wall])
          power_switch();

      color("Red"){
          hull(){
       translate([_width-34,_depth+5,_height/2.5+_wall])
          rotate([90,0,0])cylinder(d=7,h=10);
       translate([_width-40,_depth+5,_height/2.5+_wall])
          rotate([90,0,0])cylinder(d=7,h=10);
          }
      }

      color("Orange"){
          hull(){
       translate([_width-10,_depth+5,_height/2.5+_wall])
          rotate([90,0,0])cylinder(d=7,h=10);
       translate([_width-15,_depth+5,_height/2.5+_wall])
          rotate([90,0,0])cylinder(d=7,h=10);
          }
      }

      // led hole 
      color("Green"){
       translate([_width-12.5,_depth+5,_height-3*_wall])
          rotate([90,0,0])cylinder(d=5.2,h=10);
      }
             
       n=10;
       dx=((_width+2*(_wall+_tol))-40)/(n-1);
       translate([20,21, 1-_tol])
        grilley(gridNX=n,hx=(dx-5),dhx=5,dy=30,h=_wall); 

       translate([20,29, _height+_tol])
        grilley(gridNX=n,hx=(dx-5),dhx=5,dy=40,h=_wall+1); 

      translate([_wall,21, _wall+10])
      rotate([0,-90,0]){
       n=5;
       dx=(_height-20)/(n-1);
        grilley(gridNX=n,hx=(dx-4),dhx=4,dy=45,h=_wall+2); 
      }
      translate([2*_wall+_width,21, _wall+10])
      rotate([0,-90,0]){
       n=5;
       dx=(_height-20)/(n-1);
        grilley(gridNX=n,hx=(dx-4),dhx=4,dy=45,h=_wall+2); 
      }

   }//diff
   
   /*
   // fittings
       translate([24+_wall*3,_depth,_height/2+_wall])
            power_switch();
   
      color("Green"){
       translate([_width-_wall*3,_depth+5,_height-3*_wall])
          rotate([90,0,0])cylinder(d=5.2,h=10);
      }

      color("Red"){
          hull(){
       translate([_width-20,_depth+5,_height/3+_wall])
          rotate([90,0,0])cylinder(d=6,h=10);
       translate([_width-23,_depth+5,_height/3+_wall])
          rotate([90,0,0])cylinder(d=6,h=10);
          }
      }

      color("Orange"){
          hull(){
       translate([_width-38,_depth+5,_height/3+_wall])
          rotate([90,0,0])cylinder(d=6,h=10);
       translate([_width-45,_depth+5,_height/3+_wall])
          rotate([90,0,0])cylinder(d=6,h=10);
          }
      }

      color("Green"){
       translate([_width-_wall*3,_depth+5,_height-3*_wall])
          rotate([90,0,0])cylinder(d=5.2,h=10);
      }
      */

}

module far_end(){
    
   difference(){

    union(){
    difference(){
        
        union(){

       intersection(){
        translate([-1,_fardepth/2,_screwz+_wall+_tol]) rotate([0,90,0]) cylinder(d = _fardepth+2*_wall, h = _width+2*_wall+2);
        union(){
        translate([( _width+2*_wall)/2,
                   ( _fardepth+2*_wall)/2-_wall,
                   (_height+_wall)/2])
        cube([ _width+2*_wall,
                     _fardepth+2*_wall,
                    2*_wall],true);

         translate([( _width+2*_wall)/2,
                   ( _fardepth+2*_wall)/2-_wall,
                   (_height/2+_wall)/2])
        rotate([90,0,0])
        roundedBox([ _width+2*_wall,
                    _height/2+_wall,
                   _fardepth+2*_wall],4,true);
        }//u
        }//int
        
        union(){
        translate([( _width+2*_wall)/2,
                   ( _fardepth+2*_wall)/2-_wall,
                   (_screwz+2*_wall+_tol)/2+_wall])
               cube([ _width+2*_wall,
                     _fardepth+2*_wall,
                     2*_wall],true);

         translate([( _width+2*_wall)/2,
                   ( _fardepth+2*_wall)/2-_wall,
                   (_screwz+_wall+_tol)/2])
        rotate([90,0,0])
        roundedBox([ _width+2*_wall,
                    _screwz+_wall+_tol,
                   _fardepth+2*_wall],4,true);        
            }//u

       }//u
       
        translate([( _width+2*_tol)/2+_wall-_tol,
                   ( _fardepth+2*_tol)/2-_tol,
                   (_height+2*_tol)/2+_wall-_tol,])
        cube([ _width+2*_tol,
               _fardepth+2*_wall+2*_tol,
               _height+2*_tol],true);

        translate([-1,_fardepth/2,_screwz+_wall+_tol])
        rotate([0,90,0]) 
            boltHole(size=_screwM,length=10,tolerance=0.1);

        translate([_width+_wall+_tol-1,_fardepth/2,_screwz+_wall+_tol])
        rotate([0,90,0]) 
            boltHole(size=_screwM,length=10,tolerance=0.1);

        //translate([

    }

       hull(){
       translate([(_width+2*(_wall+_tol))-20,_fardepth/2, -4])
            cylinder(d1=10,d2=20,h=5);
       translate([20,_fardepth/2, -4])
            cylinder(d1=10,d2=20,h=5);
        }
    }// u
    
       translate([(_width+2*(_wall+_tol))-32-_wall,_fardepth/2, -4.1]){
            boltHole(size=_screwM,length=15,tolerance=0.1);
            washerHole(size=_screwM,length=5,tolerance=0.1);
       }
       translate([_wall+32,_fardepth/2, -4.1]){
            boltHole(size=_screwM,length=15,tolerance=0.1);
            washerHole(size=_screwM,length=5,tolerance=0.1);
       }
    
}//d

}

module power_switch(){
    color([0.2,0.2,0.2,1.0]){
        cube([48,25,28],true);
        translate([0,-2,0])cube([52,25,32],true);
    }
}

module grilley(gridNX=8,hx=8.0,dhx=2.0,dy=40,h=10){
        echo(hx,dhx);

      for(i = [0 : gridNX-1]) {
          translate([i*(hx+dhx),0,-1]) {
             hull(){
                 cylinder(d=hx,h=h);
                 translate([0,dy,0])cylinder(d=hx,h=h);
             }
          }
      }

  }
