  tol= 0.1;//mm
delta=2.54;//mm
  pin= 0.8;//mm

hallboard([0,0,0]);

module hallboard(p){

boardWidth =15.5; //mm
boardHeight=1.50; //mm
boardLength=19.0; //mm

translate(p){
difference(){
    cube([boardLength+tol,boardWidth+tol,boardHeight+tol],center=true);
    translate([0.5*boardLength-2.5, 5,-0.5*(boardHeight+1)]) cylinder(d=2,h=boardHeight+1,$fn=30);
    translate([0.5*boardLength-2.5,-5,-0.5*(boardHeight+1)]) cylinder(d=2,h=boardHeight+1,$fn=30);
    
    translate([0.5*boardLength-5,0,0])
    for (q=[[0,-delta,0],[0,0,0],[0,delta,0]]){
      translate(q+[0,0,-0.5*(boardHeight+1)]) cylinder(d=pin,h=boardHeight+1,$fn=30);
    }
    translate([0.5*boardLength-8,0,0])
    for (q=[[0,-delta,0],[0,0,0],[0,delta,0]]){
      translate(q+[0,0,-0.5*(boardHeight+1)]) cylinder(d=pin,h=boardHeight+1,$fn=30);
    }
    translate([0.5*boardLength-11.5,0,0])
    for (q=[[0,-delta,0],[0,0,0],[0,delta,0]]){
      translate(q+[0,0,-0.5*(boardHeight+1)]) cylinder(d=pin,h=boardHeight+1,$fn=30);
    }
   translate([0.5*boardLength-16.5,0,0])
    for (q=[[0,-delta,0],[0,0,0],[0,delta,0]]){
      translate(q+[0,0,-0.5*(boardHeight+1)]) cylinder(d=pin,h=boardHeight+1,$fn=30);
    }
}
  color("red") translate([0.5*boardLength-6.5,5.5,,0.5*boardHeight+0.5]) cube([2.5+tol,1.5+tol,1.0+tol],center=true);
  color("black") translate([0.5*boardLength-11.5,5.5,0.5*boardHeight+0.5]) cube([2.5+tol,1.5+tol,1.0+tol],center=true);

  color("black") translate([0.5*boardLength-16.5,0.0,0.5*boardHeight+1.25]) cube([2.5+tol,7.5+tol,2.5+tol],center=true);

   translate([0.5*boardLength-16.5,0,0])
    for (q=[[0,-delta,0],[0,0,0],[0,delta,0]]){
      translate(q+[0,0,-0.5*(boardHeight+3)]) color("silver") bentleg(d=0.6,l1=5,offset=0.5);
    }

    translate([0.5*boardLength-8,0,-0.5*(boardHeight+3)]) sensor();
    
}
}

module sensor(){
    color("silver") {
      translate([0,-delta,0]) rotate([0,0,186])  bentleg(d=0.35,l1=2.5,l2=11,offset=0.5);//cube([0.35,0.35,14.0],center=true);
      translate([0,     0,0]) rotate([0,0,180])  bentleg(d=0.35,l1=2.5,l2=11,offset=0.5);//cube([0.35,0.35,14.0],center=true);
      translate([0, delta,0]) rotate([0,0,174])  bentleg(d=0.35,l1=2.5,l2=11,offset=0.5);//cube([0.35,0.35,14.0],center=true);
    }
    color("black")translate([12.0,0,4])cube([3.5,4.0,1.5],center=true);
}

module bentleg(a=90, d=1, offset=1, l1=2, l2=7){
    $fn=90;
    translate([0,0,l1+offset+d/2])
    rotate([180,0,180]){
    translate([0,0,l1/2])cube([d,d,l1],center=true);
    translate([offset+d/2,-d/2,0]){
    rotate([0,90,90])
    rotate_extrude(angle = a, convexity = 2) {
          translate([offset,0, 0])
          square([d,d]);
    }
    }
    translate([l2/2+offset+d/2,0,-offset-d/2]) rotate([0,90,0]) cube([d,d,l2],center=true);
}
}