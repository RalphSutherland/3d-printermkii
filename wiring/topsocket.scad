use <../std/boxes.scad>;
use <../std/fasteners.scad>;


_ShowBolts = 0;

_prof = 30;

_thick = 3;

_plug_diameter  = 16; // Hot end 5 pin Mic
_plug2_diameter =  8; // obj fan DC

$fn=60;

 bracket(30,m=5);


module bracket(size=_prof, m=6){
// holes
// M5 : 5.50
// M6 : 6.60
// M8 : 8.80
//
    TOL = 0.1;
    boltd = (m==5)?(5.50+TOL):((m==6)?6.60+TOL:8.80+TOL);

    thick = _thick;
    difference(){
           translate([0,0,0])angle(size,1.5);
           translate([thick,0,thick])angle(size-thick-1,1.6);
           // base anchor
           translate([size+2,0,-1]) {
               hull(){
                  cylinder(d=boltd,h=thick+2,$fn=50);
                  translate([-2,0,0])cylinder(d=boltd,h=thick+2,$fn=50);
               }
               hull(){
                  translate([0 ,0,thick])cylinder(d=boltd+5,h=thick+2,$fn=50);
                  translate([-2,0,thick])cylinder(d=boltd+5,h=thick+2,$fn=50);
               }
           }

          //socket holes
           translate([-1,0,size+2]) {
               rotate([0,90,0]){
                  cylinder(d=_plug_diameter,h=thick+2);
             }
           }
           translate([-1,4,size-16]) {
               rotate([0,90,0]){
                  cylinder(d=_plug2_diameter,h=thick+2);
             }
           }
           
    }
  
       translate([size/2+2,0,0]){
         rotate([0,180,0]) hull(){
                cylinder(d=8,h=2); 
                translate([-4+size/2,0,0]) 
                     cylinder(d=8,h=2);}
       }

    if (_ShowBolts==1){
        color("Silver"){
            translate([size,0,0]){
                socketBolt(size=m,length=8); echo("Brace Bolt: M",m);
                translate([0,0,_thick])nut(size=m);
            }
        }
    }

}



module angle(size=_prof,ext=1.5){
    thick = _thick/2;
    translate([0,-size/2,0])
    hull(){
        cube([size*ext,size,thick]);
        translate([thick,0,0])
           rotate([0,-90,0])
               cube([size*ext,size,thick]);
        
    }
}
