use <std/boxes.scad>
use <std/fasteners.scad>
include <../const.scad>
//
// profile mount point for filament reels
//
_col = _fitColor;

_thick = 10;// bracket thickness

 reelMount();
 // yblock(w=100,h=16,l=40,p=30);
 /*
 translate([0,-12,0]){
    //translate([0,3,0])cube([16,9,40],center=true);
     reelHardware(m=8, length=140);
 }
*/

module reelMount(p=_prof){
    
    translate([0,-20,0]){
        difference(){
            color(_col) translate([0,20,0]) rotate([90,0,0]) 
                yblock(w=100,h=16,l=40,p=p, m=6);
          
             translate([0,5,0]){
                 translate([0,3,0])cube([16,8,40],center=true);
                 reelHardware(m=8, length=140);
             }
        }
        
        translate([-26,21.9,0]){
         rotate([90,0,0]) hull(){cylinder(d1=6,d2=8,h=2); translate([12,0,0]) cylinder(d1=6,d2=8,h=2);}
       }

            translate([15,21.9,0]){
         rotate([90,0,0]) hull(){cylinder(d1=6,d2=8,h=2); translate([12,0,0]) cylinder(d1=6,d2=8,h=2);}
       }


        
    }

    
}


module yblock (w=40, h=16, l=20, p=20, m=6) {
    //
    h2   = h-8;
    h4   = h+4;
    hl   = (h<l)?l:h;
    //
    difference(){
        
        union(){
            hull(){
                translate([0,0,(hl/2)]) // 
                    roundedBox([ 1.25*h,p,hl],2,true); 
                translate([0,0,(h2/2)]) 
                    roundedBox([w-h*3,p,h2],2,true);
            }
            translate([0,0,(h2/2)]) 
                roundedBox([w,p,h2],2,true);
            translate([0,0,hl]) 
                cylinder(d1=18,d2=15, h=2);
        }
        
        //
        translate([w/2-(h*3/4),0,0]){
            translate([0,0,-1])
                boltHole(size=m, length=2*h2+2);
            translate([0,0,7])
                cylinder(d1= 18, d2= 20, h=2);
        }
        translate([-w/2+(h*3/4),0,0]){
            translate([0,0,-h2-1])
                boltHole(size=m, length=2*h2+2);
            translate([0,0,7])
                cylinder(d1= 18, d2= 20, h=2);
        }
    }
    //
}

module reelHardware(m=8, length=140){
    color("Silver"){
        rotate([90,0,0]) {
            rotate([0,0,30])nutHole(size=m, tolerance=0.2);
            boltHole(size=m, length=length, tolerance=0.2);
        }
    }
}
