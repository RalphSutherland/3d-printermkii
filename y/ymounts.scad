/*
 Name: OpenSCAD model of a MaxMicron 20-20 profile Y-Axis Motor Mount

 Can generate single piece, stronger, for printing with supports, or
 two pieces to print wihtout supports. Edit one_piece as needed.

 Printed well with PLA, 5 perimeters, 5 top and bottom layers, 40% fill
 and 0.25 layers, 0.4 nozzle and 0.5mm width.

 Author: R Sutherland

 License: CC BY-SA 4.0
 License URL: https://creativecommons.org/licenses/by-sa/4.0/
 */
//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/t-slot.scad>
use <std/fasteners.scad>
use <std/motors.scad>
use <std/GT2Gear.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <../const.scad>

one_piece = true;
_Mbolt = 6;// M6, M5 or M4 holes

// dimensions - all mm
_thick = 7;// bracket thickness
_mlen  = 60; // longer motor, std = ~48
_len   = _mlen+48; // longer motor, std = ~48
_vertical = 15;

//
//======================================================================
//

_showMotor     = 1;
_showProfile   = 0;
_showHardware  = 1;

_col = _yaxisColor;

//======================================================================
// main program, comment components as needed
//======================================================================
//yrodblock(w=76, m=6, d=10, p=30);
//
/*
ymotor(length=60);
if (_showProfile == 1) {
    translate([25,-_prof/2,-_prof/2])
    rotate([0,90,0])tslotProfile(size=_prof,length=130);
}

*/


yidle(d=18,gap=9);
if (_showProfile == 1) {
    translate([25,_prof/2,-_prof/2])
    rotate([0,90,0])tslotProfile(size=_prof,length=130);
}


//translate([0,-13,0]) rotate([0,0,-90]) tensionblock(d=18, h=8.5);
//======================================================================
// modules
//======================================================================


module yidle(d=18, gap=8.5,p=_prof){
    
    translate([0,0,-_prof])
    translate([0,0,_vertical]){
       translate([0,-8,0]) rotate([90,0,0]) {
            yblock(w=70, l=30, h=16, gap=gap,d=d,p=p);
          }

       if (_showHardware == 1) {
           translate([  0,  -15-8, 15]) rotate([90,0,90]) {
                color("Silver")GT2Nidle(20, d=18, h=8.5);
            }
        }

    }
}

module ymotor(length=_mlen){
    plen = length+48;
    translate([11,0,_vertical])
    rotate([0,0,0]){
        profile_mount_full(plen);
        translate([length+_thick,_moc+_thick,-_prof/2]) rotate([-90,180,90])  {
            translate([0,0,length]) motor_mount(length);
            if ((_showHardware == 1) || (_showMotor == 1) ){
                translate([-0.5,0.5,0]){
                   if (_showMotor == 1) rotate([0,0,-90]) stepper_motor(17,length=length);
                    color("Silver") if (_showHardware == 1){
                    translate([0,0,length]){
                    translate([0,0,18])GT2N(20);
                    translate([ 15.5, 15.5,2*_thick]) rotate([180,0,0]) socketBolt(size=3,length=20,tolerance=_tol);
                    translate([-15.5,-15.5,_thick])   rotate([180,0,0]) socketBolt(size=3,length=12,tolerance=_tol);
                    translate([-15.5, 15.5,_thick])   rotate([180,0,0]) socketBolt(size=3,length=12,tolerance=_tol);
                    translate([ 15.5,-15.5,2*_thick]) rotate([180,0,0]) socketBolt(size=3,length=20,tolerance=_tol);
                    }
                }
                }
            }
        }
    }

}

function yrodblockheight (d=8, t=8)  = d+t ;
function yrodblockwidth  (d=8, t=8)   = 6*d ;
function yrodblockbelow  (d=8, t=8)  = (d + t)/2;

module yrodblock(d=8, m=5, t=8, p=20){

    wid = (p==30)?8:6;
    color(_col)
    difference(){
        union(){
            yrodmount (w=6*d, h=2*d, t=t, d=d, p=p);
            translate([ 0,0, -(t+d)/2-1]){
                hull(){
                     translate([ -d/2, 0, 0]) cylinder(d=wid,h=2);
                     translate([  d/2, 0, 0]) cylinder(d=wid,h=2);
                }
            }
        }
        rotate([90,0,0]) translate([0,0,-(p+2)/2]) cylinder(d= d+_tol, h=p+2);

       translate([ 2*d,0,0]) {
          translate([ 0,0,-(t+d)/2-1]) boltHole(size=m,length=d+t+2,tolerance=_tol);
          translate([ 0,0, (t-d)/2-1]) washerHole(size=m,length=d+t,tolerance=_tol);
       }

       translate([-2*d,0,0]) {
          translate([ 0,0,-(t+d)/2-1]) boltHole(size=m,length=d+t+2,tolerance=_tol);
          translate([ 0,0, (t-d)/2-1]) washerHole(size=m,length=d+t,tolerance=_tol);
       }

    }
}

module yrodmount (w=40, h=16, d=8, t=8, p=20) {

    //
    // idle block
    //
     color(_col)hull(){
        translate([ 0, 0,  d/2])  roundedBox([ 1.25*h, p, t], 2, true); //cube([h,p,h],center=true);
        translate([ 0, 0, -t/2])  roundedBox([ 2*h, p, t], 2, true);
      }

      translate([0,0, -d/2]) roundedBox([w,p,t],2,true);

}

module yblock (w=40, h=16, l=20, gap=8.5, d=18, p=20) {


    length = 70;
    height = 44;// +6 = 50
    xlen = 40.0;

    difference(){
        union(){

    difference(){
        union(){

    // mounting plates
    color(_col) translate([0,_prof,-8])
    rotate([90,0,180]){

            // backplate
            translate([0, _thick/2,-20])   rotate([90,0,0]) roundedBox([length,_thick+height,_thick,],5,true);//cube([length,_thick,_thick+height]);

            //profile shelf
            translate([0,-_prof/2+_thick/2,-_vertical+_thick/2]) roundedBox([length,_prof+_thick,_thick],5,true);//cube([length,_prof,_thick]);
            translate([0,-(_prof+5)/2+5,-((_vertical+5)/2)+5]) rotate([0,90,0]) roundedBox([_vertical+5,_prof+5,_thick,],5,true); //cube([_thick,_prof,_vertical-1]);


    }
    //
    // idle block
    //
     translate([0,_vertical,-5]){
     h2   = h-4;
     h4   = h+4;
     hl   =  (h<l)?l:h;
    //
     color(_col)hull(){
        translate([0,0, (hl-h)/2+6]) roundedBox([ 1.5*h,p,hl],2,true); //cube([h,p,h],center=true);
        translate([0,0,2]) roundedBox([w-h*2,p,h2/2],2,true);
      }
      }
      } //  union

     translate([0,_vertical,0]){

     // cutout for idle wheel and axels
           translate([  0,  0, 15]) rotate([90,0,90]) union(){
                translate([  0,  0, -gap/2]) cylinder(d=d+2,h=gap+2*_tol);
                translate([  0, 4,    0]) cube([p+2,d+2,gap+2*_tol],center=true);
            }

        }
    //
     } // diff


      translate([  0,  0, 15]) rotate([90,0,90]) union(){

          // mount points
          translate([15,0,-(gap+1)/2])rotate([   0,0,0])cylinder(d1=12,d2=10,h=1);
          translate([15,0, (gap+1)/2])rotate([ 180,0,0])cylinder(d1=12,d2=10,h=1);

        }

    }//union

     // bolt holes axle
           translate([  0,  15, 15]) rotate([90,0,90]) union(){
               m=3;
                translate([  0,  0,-12.5]) boltHole(size=m,length=25, tolerance=_tol);
                translate([  0,  0,  -18]) washerHole(size=m,length=6, tolerance=_tol);
                //translate([  0,  0,   10]) nutHole(size=m, tolerance=_tol);
                translate([  0,  0,   12]) nutHole(size=m,tolerance=_tol);
                translate([  0,  0,   14]) nutHole(size=m,tolerance=_tol);
            }

     // bolt holes mounting
        translate([0,_prof/2,-8]){

                translate([ -60+length-10,-_prof/2,-1]) boltHole(5,length=30,tolerance=_tol);
                translate([ -60+length-10,-_prof/2,5])  washerHole(5,length=30,tolerance=_tol);//cylinder(d=2*_Mbolt,h=30,tolerance=_tol);

       // top
        translate([-20,_vertical+1,-_prof/2])  rotate([90,0,0]) {
            translate([ 0, 0, _thick+1]) washerHole(_Mbolt,length=2,tolerance=_tol);
            translate([ 0, 0, 8]) boltHole(_Mbolt,length=10,tolerance=_tol);
        }

        translate([ 20,_vertical+1,-_prof/2])  rotate([90,0,0]) {
            translate([ 0, 0,  _thick+1]) washerHole(_Mbolt,length=2,tolerance=_tol);
            translate([ 0, 0, 8]) boltHole(_Mbolt,length=10,tolerance=_tol);
        }

        }

     }// diff

      if (_showHardware){
           translate([  0,  15, 15]) rotate([90,0,90]) {
               color("Silver") translate([  0,  0,-12]) socketBolt(3,length=25,tolerance=_tol);
            }

     // bolt holes mounting
        translate([0,_prof/2,-8]){
            translate([ -60+length-10,-_prof/2,5]) rotate([180,0,0]) socketBolt(5,length=12,tolerance=_tol);
        }


        translate([-20,_prof-2,-_prof/2-8])  rotate([90,0,0]) {
            translate([ 0, 0,  _thick+1]) socketBolt(_Mbolt,length=12,tolerance=_tol);
        }
        translate([ 20,_prof-2,-_prof/2-8])  rotate([90,0,0]) {
            translate([ 0, 0,  _thick+1]) socketBolt(_Mbolt,length=12,tolerance=_tol);
        }


    }

}



module motor_mount(length=_mlen){
    translate([0,0,0]) {
        difference(){

           color(_col)  union(){

               translate([_thick/2,-_thick/2-_moc/2,_thick/2])  cube([2*_moc+_thick,_moc+_thick,_thick],center=true);
                translate([(_thick+_moc)/2,0,_thick])  cube([_moc+_thick,2*(_moc+_thick),2*_thick],center=true);
                translate([_thick/2,0,_thick/2])  roundedBox([2*_moc+_thick,2*(_moc+_thick),_thick],3,true);
                translate([0,-_thick/2-_moc/2,_thick/2])  cube([2*_moc,_moc+_thick,_thick],center=true);
               // bottom plate, shrink for better heat dissipation
               // # translate([_thick/2,-_moc-_thick/2,-(length-5)/2+_thick]) rotate ([0,90,90]) roundedBox([length-5,2*_moc+_thick,_thick],3,true);
                translate([_thick/2,-_moc-_thick/2,-(15)/2+_thick]) rotate ([0,90,90]) roundedBox([15,2*_moc+_thick,_thick],3,true);

            }

            translate([-0.5,0.5,-_thick]) rotate ([0,0,0])  stepper_motor_mount(17,slide_distance=0, mochup=false, tolerance=_tol);
            translate([-_tol,_tol,-_thick]) rotate ([0,0,0])  cylinder(d=24,h=4*_thick); // slightly larger hole for some steppers

        }

    } // trans
}

module profile_mount_full(length=71, height=_mod){
    xlen = 40.0;
    zoff = _vertical; // top plate offset
    hg=2*_thick+height;
    difference(){
        color(_col) union(){
            // backplate
            translate([length/2-xlen, _thick/2,-zoff])   rotate([90,0,0])      //hg/2-_vertical-_prof
                  roundedBox([length,hg,_thick,],5,true);//cube([length,_thick,_thick+height]);

            //profile shelf
            translate([length/2-xlen,-_prof/2+_thick/2,-_vertical+_thick/2]) 
                  roundedBox([length,_prof+_thick,_thick],5,true);//cube([length,_prof,_thick]);
            // ribs
            translate([-(xlen*0.75)/2,7.5,hg/2-zoff-_thick/2]) roundedBox([xlen*0.75,15,_thick],5,true);
            translate([-(xlen*0.75)/2,7.5,-zoff]) roundedBox([xlen*0.75,15,_thick],5,true);
             translate([-(xlen*0.75)/2,7.5,-hg/2-zoff+_thick/2]) roundedBox([xlen*0.75,15,_thick],5,true);

            translate([ -40+3*length/4  ,-_prof/2+5,-5]) rotate([90,0,90]) roundedBox([xlen*0.75,20,_thick],4,true);
            translate([ -40+length/4     ,-_prof/2+5,-5]) rotate([90,0,90]) roundedBox([xlen*0.75,20,_thick],4,true);

        }

        // bolt holes
        translate([0,0,-zoff]){
            
                translate([ -40+length-12  ,-_prof/2,-1]) boltHole(_Mbolt,length=10);
                translate([ -40+length-12  ,-_prof/2,_thick-1]) cylinder(d=2.2*_Mbolt+_tol,h=2);
            
                translate([ -40+length/2  ,-_prof/2,-1]) boltHole(_Mbolt,length=10);
                translate([  -40+length/2  ,-_prof/2,_thick-1]) cylinder(d=2.2*_Mbolt+_tol,h=2);
            
                translate([-40+12  ,-_prof/2,-1]) boltHole(_Mbolt,length=10,tolerance=_tol);
                translate([ -40+12  ,-_prof/2,_thick-1]) cylinder(d=2.2*_Mbolt+_tol,h=2);

               translate([ -30,10+_thick,-_prof/2])  rotate([90,0,0]) {
                translate([ 0, 0, 9]) cylinder(d=2*_Mbolt,h=2);
                translate([ 0, 0, 8]) boltHole(_Mbolt,length=10);
            }

            // hidden M5 bolt
            translate([ 55,10+_thick,-_prof/2])  rotate([90,0,0]) {
                translate([ 0, 0, 9]) cylinder(d=2*5+_tol,h=6);
                translate([ 0, 0, 8]) boltHole(5,length=10,tolerance=_tol);
            }

            }


    }

    if (_showHardware == 1){
        color("Silver") translate([0,0,-zoff]){
            translate([ -40+length-12,-_prof/2,-1]) rotate([180,0,0]) {
               translate([ 0, 0, -6])  socketBolt(_Mbolt,length=15,tolerance=_tol);
            }

            translate([ -40+length/2,-_prof/2,-1])  rotate([180,0,0]) {
                translate([ 0, 0, -6])  socketBolt(_Mbolt,length=15,tolerance=_tol);
            }

            translate([-40+12,-_prof/2,-1])  rotate([180,0,0]){
                translate([ 0, 0, -6])  socketBolt(_Mbolt,length=15,tolerance=_tol);
            }

            translate([ -30,10+_thick,-_prof/2])  rotate([90,0,0]) {
                translate([ 0, 0, 11]) socketBolt(_Mbolt,length=15,tolerance=_tol);
            }
            // hidden M5 bolt
            translate([ 55,10+_thick,-_prof/2])  rotate([90,0,0]) {
                translate([ 0, 0, 15]) socketBolt(5,length=10,tolerance=_tol);
            }

        }
    }
}

module profile_mount_motor(){
    difference(){
        union(){
            translate([_thick,0,-27.2]) cube([24.0,_thick,_mod]);
            translate([_thick,-20,0]) cube([24,20,_thick]);
        }
        hull(){
            translate([ 20,-10,-1]) boltHole(_Mbolt,length=_thick+2,tolerance=_tol);
            translate([ 22,-10,-1]) boltHole(_Mbolt,length=_thick+2,tolerance=_tol);
        }

    }

}
