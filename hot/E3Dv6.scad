//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <E3Dv6_fans.scad>
use <E3Dv6_mount.scad>

//======================================================================
// Parameters all dimensions in mm
//======================================================================

    $fn=60;

    _tol=0.1;
    _ShowFittings = 0;

    radiator_diameter  = 22.0;
    radiator_top       = 42.75;
    radiator_fittingHeight = 12.75;

    extruder_height=radiator_top-0.25;//42.5 to top of bracket, less small extend

//======================================================================
// main program, comment components as needed
//======================================================================

 //   E3Dv6_extruder();
//    E3Dv6_extruderMounted(fan = 0);
    E3Dv6_extruderMounted();

//======================================================================
// modules
//======================================================================

module E3Dv6_extruderMounted(){
     translate([ 0, 0, -10]) rotate([0,0,0]) E3Dv6_extruder();
     translate([-20, 0, extruder_height])
         E3Dv6_Mount();
     translate([0, 0, 3])  rotate([0,0,-90])
         E3Dv6_StdFanMount();
}

module E3Dv6_extruder(){


     translate([0, 0,  radiator_top]) E3Dv6_topFitting();
     translate([0, 0,             0]) E3Dv6_radiator();
     translate([0, 0,   -_tol-(5+2)]) E3Dv6_thermalBarrel();
     translate([0, 0,        -(1+2)]) E3Dv6_heater_v2();
     translate([0, 0,-(11.5 +5+1+2)]) E3Dv6_M6Nozzle();

}

module E3Dv6_topFitting(){
        translate([0,0,-1.0])
        difference(){
        union(){
          translate([0,0,0.5])   color("white")  cylinder(d=4,h=10); // PTFE tube
          translate([0,0,1])  color("Black")  translate([0,0,0]) cylinder(d=7.0, h=2); // inlet
        }
         cylinder(d=2,h=11); // PTFE tube
        }
}

module E3Dv6_M6Nozzle(){
//
// h = 12.5
// dz to top of nut = 5
//
     difference(){
     color("DarkGoldenrod") union(){
          translate([ 0, 0, 0]) cylinder(d1=1.00, d2=6.0, h=3.0);
          translate([ 0, 0, 2]) cylinder(d=8.00,h=3.0,$fn=6);
          translate([ 0, 0, 5]) cylinder(d=5.00,h= 7.50);
          translate([ 0, 0, 7]) cylinder(d=6.00,h= 5.50);
     }
     translate([ 0, 0, -1]) cylinder(d=0.5,h=15);
     translate([ 0, 0, 6]) cylinder(d=2.0,h=10);
 }

}

module E3Dv6_thermalBarrel(){
// h = 22
    difference() {
        color("DarkGray")
        union() {
            cylinder(d=6,h= 5);
            cylinder(d=3,h=20);
            translate([ 0, 0, 7]) cylinder(d=6.9,h=9);
            translate([ 0, 0, 7]) cylinder(d=5.8,h=13);
        }
        translate([ 0, 0, -1])   cylinder(d=2.0,h=22);
        translate([ 0, 0, 14])   cylinder(d1=2.0,d2=4.1,h=2.1);
        translate([ 0, 0, 16])   cylinder(d=4.0,h=6);
    }
}

module E3Dv6_radiator(){
// h = 42.75

    radiator_fin_thick = 1.0;
    radiator_fin_dz    = 2.5;
    radiator_shaft     = radiator_top - radiator_fittingHeight;

    difference(){

        color("lightgray") union(){

            cylinder(d1=12.5,d2=9,h=radiator_shaft );
            translate([ 0, 0, radiator_top-3.75]) cylinder(d=16,h= 3.75); // top=42.75
            translate([ 0, 0, radiator_shaft    ]) cylinder(d=12,h=radiator_fittingHeight); // top=42.75
            translate([ 0, 0, radiator_shaft    ]) cylinder(d=16,h= 3.00); // bottom=42.75-12.75
            translate([ 0, 0, radiator_shaft-radiator_fin_dz ]) cylinder(d=16,h= 1.00); // bottom=42.75-16

            for (i = [0:10]) {
                translate([0,0,radiator_fin_dz*i]) cylinder(d=radiator_diameter,h=radiator_fin_thick);
            }
        }

        translate([ 0, 0, -1 ])cylinder( d=6, h=radiator_top+2);
        translate([ 0, 0, 22 ])cylinder( d=4, h=radiator_top+2);
        translate([ 0, 0, 30 ])cylinder( d=8, h=radiator_top+2);

    }

    difference(){
        dh=3.75;
        color("Goldenrod") translate([ 0, 0, radiator_top-dh]) {
          intersection(){
              cylinder(d=8,h=dh); // top=42.75
              union(){
                 cylinder(d1=7,d2=7+dh,h=dh/2); // top=42.75
                 translate([ 0, 0, dh/2]) cylinder(d1=7+dh,d2=7,h=dh/2); // top=42.75
              }
          }
        }
        translate([ 0, 0, 22 ])cylinder( d=4, h=radiator_top+2);
    }

    if (_ShowFittings==1){
        color("Gray") rotate([180,0,0]) translate([0,0,3]){
            translate([-9, 6.5, 7.5])  rotate([0,90,0]) cylinder(r=3.05, h=18);
            translate([ 0,12.5,-1.0])  cylinder(r=1.4, h=13.5);
            translate([ 0,-6.0, 5.75]) cylinder(r=1.2, h=6);
            translate([-9,-6.0, 5.75]) rotate([0,90,0]) cylinder(r=1.55, h=18);
        }
    }

}

module E3Dv6_heater_v2(rotate=-90) {
// h = 11.5
    rotate([180,0,rotate]) difference() {
        color("Silver") union() {
            translate([-8,-8,0]) chamferBox([16,23,11.5],delta=0.4);
        }
        // heated block
        translate([0,0,-1])     cylinder(d=5.50,h=14);
        translate([-9,6.5,7.5]) rotate([0,90,0]) cylinder(r=3.05, h=18);
        translate([-9,8,6.5])   cube([18,8.5,2]);
        translate([0,12.5,-1])  cylinder(r=1.4, h=13.5);
        translate([0,-6,5.75])  cylinder(r=1.2, h=6);
        translate([-9,-6,5.75]) rotate([0,90,0]) cylinder(r=1.55, h=18);
    }
}

