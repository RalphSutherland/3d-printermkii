//======================================================================
// Uses
//======================================================================

 use <std/boxes.scad>
 use <std/fasteners.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

 include  <../const.scad>

// Mounting bolt holes - M4 bolts through the print bed base into the top of the clamp
mount_hole_spacing  = 40; // space between the hole centres in y dir
mount_hole_spacingx = 35; // space between the hole centres in x dir
mount_bolt_size      = 5; // 4 = M4, or 5 = M5 fittings

// clamp dimensions
clamp_y = 60; // length of the clamp along the y axis
clamp_z = 25; // depth between the bed and the top of the belt
clamp_plate_thickness = 7; // Thickness of the mounting plate at the top of the plate, needs to be quite thick so it's rigid
clamp_side_thickness  = 6; // Thickness of the rest of the plates that make up the bracket

// Variables for the two belt clamps

belt_clamp_height    =   9.5;
belt_clamp_width     =    28;
belt_clamp_chamfer   =     2;
belt_clamp_depth     =    10;
belt_tooth_diameter  =  1.25;
belt_tooth_depth     =  1.75;
belt_clamp_gap       =     1.0;
belt_clamp_end_gap   =     4;
belt_pitch           =     2;

// Calculated values
clamp_x = ((belt_clamp_depth - belt_clamp_chamfer) / 2 + clamp_side_thickness) * 2.5; // width of the clamp plate
side_offset = (belt_clamp_depth - belt_clamp_chamfer) / 2 + clamp_side_thickness / 2;
side_z = clamp_z - clamp_plate_thickness + belt_clamp_height;
infill_z = clamp_z - clamp_plate_thickness - clamp_side_thickness - belt_clamp_gap;

_col = _yaxisColor;

//======================================================================
// main program, comment components as needed
//======================================================================

yclamp();
//yclampTop();
//yclampBase();
//======================================================================
// modules
//======================================================================

function yclampheight()=clamp_z;
function yclampwidth()=clamp_y+20;

module yclampTop(){

rotate([0,90,90])
difference() {
    color(_col)
    union(){
    translate([side_offset, 0, -belt_clamp_height+(belt_clamp_height+belt_clamp_gap+3)/2]){
        rotate([0,90,0])roundedBox([belt_clamp_height+belt_clamp_gap+3,clamp_y,clamp_side_thickness],3,true);
    }

    // belt clamps and block
    translate([0,0,clamp_side_thickness /2 + belt_clamp_gap]) {
        difference(){
        cube([belt_clamp_depth*2, clamp_y, clamp_side_thickness],center=true);
        translate([0,-20,1])cylinder(d=8.4,h=2.2);
        translate([0, 0,1])cylinder(d=8.4,h=2.2);
         translate([0,20,1])cylinder(d=8.4,h=2.2);
        }
    }
    translate([0,clamp_y / 2 - belt_clamp_width / 2,0])
    belt_clamp();
    translate([0,-clamp_y / 2 + belt_clamp_width / 2,0])
    belt_clamp();
}
 //   #mounting_holes(dx=mount_hole_spacingx/2,dy=mount_hole_spacing/2);
// extra holes for two part version
    translate([-7,0,0]) {
        cylinder(d=4, h=20,$fn=6);
         translate([0,0,-10])washerHole(size=3, length=10, tolerance=0.2);
    }
    translate([7,0,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
    translate([7,20,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
    translate([7,-20,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }

}

}

module yclampBase(){

    difference() {
    color(_col)
    union(){
    translate([0,0,clamp_z - clamp_plate_thickness / 2]){
       hull(){
           roundedBox([clamp_x*2, clamp_y, clamp_plate_thickness],3,true);
           roundedBox([clamp_x, clamp_y*1.25, clamp_plate_thickness],3,true);
       }
    }
    hull(){
        translate([0,(clamp_y  - clamp_side_thickness) / 2,infill_z /2 + belt_clamp_gap + clamp_side_thickness]){
            cube([belt_clamp_depth*2, clamp_side_thickness, infill_z], center = true);
            translate([0,-(clamp_y  - clamp_side_thickness) ,0])
                 cube([belt_clamp_depth*2, clamp_side_thickness, infill_z], center = true);
        }
        translate([0,0,clamp_z - clamp_plate_thickness / 2])
            roundedBox([clamp_x, clamp_y*1.25, clamp_plate_thickness],3,true);
    }
        translate([0,0,belt_clamp_gap + clamp_side_thickness-2]){
         translate([0,-20,0])cylinder(d=8,h=2);
         cylinder(d=8,h=2);
          translate([0,20,0])cylinder(d=8,h=2);
        }
   }

    mounting_holes(dx=mount_hole_spacingx/2,dy=mount_hole_spacing/2);

// extra holes for two part version
    translate([-7,0,0]) {
        cylinder(d=4, h=20,$fn=6);
         translate([0,0,-10])washerHole(size=3, length=10, tolerance=0.2);
    }
    translate([7,0,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
    translate([7,20,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
    translate([7,-20,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
}

}

module yclamp(){

rotate([0,0,0])
difference() {
    clamp();
    mounting_holes(dx=mount_hole_spacingx/2,dy=mount_hole_spacing/2);
// extra holes for two part version
    translate([-7,0,0]) {
        cylinder(d=4, h=20,$fn=6);
         translate([0,0,-10])washerHole(size=3, length=10, tolerance=0.2);
    }
    translate([7,0,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
    translate([7,20,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }
    translate([7,-20,-10]) {
        cylinder(d=3, h=33,$fn=6);
    }

}
}

module clamp()
{
    color(_col) {

        translate([0,0,clamp_z - clamp_plate_thickness / 2]){

        hull(){
                roundedBox([clamp_x*2, clamp_y, clamp_plate_thickness],3,true);
                roundedBox([clamp_x, clamp_y*1.25, clamp_plate_thickness],3,true);
        }
    }
    hull(){
        translate([0,(clamp_y  - clamp_side_thickness) / 2,infill_z /2 + belt_clamp_gap + clamp_side_thickness]){
            cube([belt_clamp_depth*2, clamp_side_thickness, infill_z], center = true);
            translate([0,-(clamp_y  - clamp_side_thickness) ,0])
                 cube([belt_clamp_depth*2, clamp_side_thickness, infill_z], center = true);
        }
        translate([0,0,clamp_z - clamp_plate_thickness / 2])
            roundedBox([clamp_x, clamp_y*1.25, clamp_plate_thickness],3,true);
    }
        translate([0,0,belt_clamp_gap + clamp_side_thickness-2]){
         translate([0,-20,0])cylinder(d=8,h=2);
         cylinder(d=8,h=2);
          translate([0,20,0])cylinder(d=8,h=2);
        }


  // side


    translate([side_offset, 0, -belt_clamp_height+(belt_clamp_height+belt_clamp_gap+3)/2]){
        rotate([0,90,0])roundedBox([belt_clamp_height+belt_clamp_gap+3,clamp_y,clamp_side_thickness],3,true);
    }

    // belt clamps and block
    translate([0,0,clamp_side_thickness /2 + belt_clamp_gap]) {
        difference(){
        cube([belt_clamp_depth*2, clamp_y, clamp_side_thickness],center=true);
        translate([0,-20,1])cylinder(d=8.4,h=2.2);
        translate([0, 0,1])cylinder(d=8.4,h=2.2);
         translate([0,20,1])cylinder(d=8.4,h=2.2);
        }
    }
    translate([0,clamp_y / 2 - belt_clamp_width / 2,0])
    belt_clamp();
    translate([0,-clamp_y / 2 + belt_clamp_width / 2,0])
    belt_clamp();

    }
}

module belt_clamp()
{
    points = [
        [0, 0, 0],
        [0, belt_clamp_height - belt_clamp_chamfer, 0],
        [0, belt_clamp_height - belt_clamp_chamfer, belt_clamp_depth],
        [0, belt_clamp_chamfer, belt_clamp_depth],
        [0, 0, belt_clamp_depth - belt_clamp_chamfer],
        [belt_clamp_chamfer, belt_clamp_height, belt_clamp_depth],
        [belt_clamp_chamfer, belt_clamp_height, 0],

        [belt_clamp_width, 0, 0],
        [belt_clamp_width, belt_clamp_height - belt_clamp_chamfer, 0],
        [belt_clamp_width, belt_clamp_height - belt_clamp_chamfer, belt_clamp_depth],
        [belt_clamp_width, belt_clamp_chamfer, belt_clamp_depth],
        [belt_clamp_width, 0, belt_clamp_depth - belt_clamp_chamfer],
        [belt_clamp_width - belt_clamp_chamfer, belt_clamp_height, belt_clamp_depth],
        [belt_clamp_width - belt_clamp_chamfer, belt_clamp_height, 0],

    ];

    faces = [
        [0,1,2,3,4],
        [1,6,5,2],
        [7,11,10,9,8],
        [8,9,12,13],
        [6,13,12,5],
        [5,12,9,10,3,2],
        [3,10,11,4],
        [0,4,11,7],
        [0,7,8,13,6,1],
    ];

    rotate([-90,0,90])
    translate([-belt_clamp_width / 2, 0, -(belt_clamp_depth - belt_clamp_chamfer) / 2])
    difference() {
        polyhedron(points = points, faces = faces);
        belt_teeth();
    }
}
/*
module belt_teeth()
{
    for (i = [belt_tooth_diameter:belt_pitch:belt_clamp_width + belt_tooth_diameter]) {
        translate([i, 0, 0])
            cylinder(d = belt_tooth_diameter, h = belt_clamp_depth);
    }
}
*/
module belt_teeth()
{
    for (i = [belt_tooth_diameter:belt_pitch:belt_clamp_width + belt_tooth_diameter]) {
        translate([i-belt_tooth_diameter, 0, 0])
        hull(){
            cylinder(d = belt_tooth_diameter, h = belt_clamp_depth);
            translate([0,belt_tooth_depth- belt_tooth_diameter, 0])cylinder(d = belt_tooth_diameter, h = belt_clamp_depth);
        }
    }
}

module mounting_holes(dx=15, dy=20)
{
   // echo(dx*2,dy*2);
    translate([0,dy, clamp_z]){
    translate([-dx,0,0]) mounting_hole(mount_bolt_size);
    translate([ dx,0,0]) mounting_hole(mount_bolt_size);
    }
    translate([0,-dy, clamp_z]){
    translate([-dx,0,0]) mounting_hole(mount_bolt_size);
    translate([ dx,0,0]) mounting_hole(mount_bolt_size);
    }
}

module mounting_hole(m=4)
{
    translate([ 0, 0, -clamp_plate_thickness-1])
    boltHole(size=m, length=clamp_plate_thickness+2,tolerance = 0.1);
    translate([ 0, 0,-clamp_plate_thickness-1])
    nutHole(size=m, tolerance = 0.2);
}
