use <std/boxes.scad>
use <std/fasteners.scad>
include <../const.scad>

//
  wid1 =  36.0;
  wid2 = _prof;
  vert1 = 12;
  vert2 = 45;
  thick =  8; // mm
// main 3030 M6 bolt holes
  hole =  6; // M6
// switch holes:
  sw_width = 9.5; // mm hole separation
  sw_depth =   7; // mm holes from leading edge
  sw_bolt  =   2;   // M2 fitting

  zendstop();
//print();

module print(){
  translate([0,vert2,0])
      rotate([90,0,0])
          zendstop();
}

module zendstop(){

  translate([0,thick/2,vert2])
      rotate([-90,0,0])
          difference(){

              // body
              union(){
                  translate([wid1/2,vert1*0.5,0])
                      roundedBox([wid1,vert1,thick], 3, true);
                  translate([0,vert2/2,0])
                      roundedBox([wid2,vert2,thick], 3, true);
              }

              // profile mount holes
              color("yellow") {

                  translate([0,wid2/3,thick*0.5+0.5])
                    rotate([180,0,0]) {
                         boltHole(hole,length=24);
                         washerHole(hole,length=1);
                    }

                  translate([0,vert2-(wid2/3),thick*0.5+0.5])
                      rotate([180,0,0]) {
                         boltHole(hole,length=24);
                        washerHole(hole,length=1);
                      }

              }// y

              // sensor mount holes
              color("red") {
                  hull(){
                    translate([wid1-(vert1*0.5)-0.25,sw_depth,thick*0.5+0.5])
                        rotate([180,0,0])
                            boltHole(sw_bolt,length=thick*2);
                    translate([wid1-(vert1*0.5)+0.25,sw_depth,thick*0.5+0.5])
                        rotate([180,0,0])
                            boltHole(sw_bolt,length=thick*2);
                  }
                  hull(){
                    translate([wid1-(vert1*0.5)-0.25 -sw_width,sw_depth,thick*0.5+0.5])
                        rotate([180,0,0])
                            boltHole(sw_bolt,length=thick*2);
                    translate([wid1-(vert1*0.5)+0.25 -sw_width,sw_depth,thick*0.5+0.5])
                        rotate([180,0,0])
                            boltHole(sw_bolt,length=thick*2);
                  }
              }

          }//diff


}

