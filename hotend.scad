//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>;
use <std/lmxuu.scad>;
use <std/fans.scad>;

use <x/xcarriage.scad>;
use <x/xblower.scad>;

use <hot/E3Dv6.scad>;

use <hot/irprox.scad>;

//======================================================================
// Parameters all dimensions in mm
//======================================================================
include <const.scad> // uses _rodsep
//======================================================================

bolt_y_centres = _carriageBoltSpacing;//24mm
bolt_x_centres = _carriageBoltSpacing;//24mm

cwidth  = _carriage_width;
cheight = _carriage_height;
rods    = _rodsep; // 44.5/45

//======================================================================
// main program, comment components as needed
//======================================================================

hotend();

//======================================================================
// modules
//======================================================================

module hotend(rods=_rodsep){

      plate_zshift = 11;
      translate([   0,0,cheight/2])
         rotate([  180,0,0])
           rotate([  0,-90,0])
             rotate([  0,0,-90])
               x_carriage();

      translate([   3, 0, -plate_zshift])
         rotate([  0,0, 90])
           x_plate(-plate_zshift);

       translate([22, 0, 8.5]) rotate([  0,0,0]) E3Dv6_extruderMounted();
       translate([27,-15.5,34.5]) rotate([0,-90,0])socketBolt(size=4,length=15);
       translate([27, 15.5,34.5]) rotate([0,-90,0])socketBolt(size=4,length=15);

       translate([7,-bolt_x_centres/2, 22]) rotate([0,-90, 0])socketBolt(size=3,length=30);
       translate([7, bolt_x_centres/2, 22]) rotate([0,-90, 0])socketBolt(size=3,length=30);
       translate([7,-bolt_x_centres/2, 22+bolt_y_centres]) rotate([0,-90, 0])socketBolt(size=3,length=30);
       translate([7, bolt_x_centres/2, 22+bolt_y_centres]) rotate([0,-90, 0])socketBolt(size=3,length=30);

       translate([49,0,0]) rotate([ 90,0, 90])
          irprox();

    dz = 0.5*(cheight-rods);

    translate([-10.5, 13.25,dz]) lmxuu(id=8);
    translate([-10.5,-13.25,dz]) lmxuu(id=8);

    translate([-10.5, 13.25,dz+_rodsep]) lmxuu(id=8);
    translate([-10.5,-13.25,dz+_rodsep]) lmxuu(id=8);

// in x-carriage.scad
    translate([-34.5,0,26.0]) rotate([90,180,-90])  {
        blower_top_mount(bolt_x_centres,bolt_y_centres);
        blower_50mm();
        blower_duct(20);
    }

}

