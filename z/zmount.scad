//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/fasteners.scad>
use <std/motors.scad>
use <std/t-slot.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <../const.scad>

_hole    = 5; //M5/6 mounting bolt

_boltspacing    = 48; //M5 mounting bolt spacing in y

_straightrod_diam    =  _zdiam;
_straightrod_offsetx =  -_rodoffsetx;
_straightrod_offsety =   _rodoffsety; //mm 6.0 MaxMicron, HICtop

_corner   =   3.0   ; // mm radius of rounded corners

_bearing_diameter = 22.0; // mm
_bearing_depth     = 7.0; // mm

_leadscrew_diam = 8.0;

//
//======================================================================
//

_showBolts = 1;

//======================================================================
// main program, comment components as needed
//======================================================================

zplatetop(prof=30);
//translate([15,0,0])rotate([0,180,0]) rotate([0,0,90]) cornerBrace(size=30);
//zplatebase(prof=_prof,h=3);
//translate([0,0,3])zplate(prof=_prof);

//======================================================================
// modules
//======================================================================

module zplateleft(prof=_prof){
    zplate(prof=prof,h=_hole);
}
module zplateright(prof=_prof){
    mirror([1,0,0]) zplate(prof=prof,h=_hole);
}
//mirror([1,0,0]) zplate();
//#rotate([0,0,180])r_end();
//translate([0,0,300]) rotate([0,0,180]) ztopplate(1);

module zplatebase(prof=_prof, h=3, m=5){

    _xsize    = 75;// 2*(_moc) + prof + 1; // mm
    _ysize    = 2*(_moc) + 18; // mm
    _zsize    =   h   ; // mm

    difference(){

     translate([_xsize/2,_ysize/2,_zsize/2]) roundedBox([_xsize,_ysize,_zsize],_corner,true);
    // echo("Plate",[_xsize,_ysize,_zsize]);
        translate([prof+_moc+1,_ysize/2,-1]) {
        //echo("Motor",[prof+_moc+1,_ysize/2,-1]);
        translate([0,0,0]) stepper_motor_mount(17,slide_distance=0, mochup=false, tolerance=_tol);

        //echo("Rod",[prof+_moc+1,_ysize/2,-1]+[_straightrod_offsetx,_straightrod_offsety,0]);        translate([_straightrod_offsetx,_straightrod_offsety,0]) cylinder(d=_straightrod_diam+_tol, h=_zsize+2);
        translate([0,0,0]) cylinder(d=24,h=_zsize+2);
    }

    translate([prof/2,_ysize/2,-1]) {
        translate([ 0,(_ysize-_boltspacing/2)/2,-1])   boltHole(m,length=_zsize+3,tolerance=_tol);
        //echo("B1",m,[prof/2,_ysize/2,-1]+[ 0,(_ysize-_boltspacing/2)/2,-1]); 
        translate([ 0,-(_ysize-_boltspacing/2)/2,-1])  boltHole(m,length=_zsize+3,tolerance=_tol);
        //echo("B2",m,[prof/2,_ysize/2,-1]+[ 0,-(_ysize-_boltspacing/2)/2,-1]); 
    }
    
  }
  

}

module zplate(prof=_prof, h=5, m=5){
 
  _xsize    = 2*(_moc) + prof + 1; // mm
  _ysize    = 2*(_moc) + 18; // mm
  _zsize    =   h   ; // mm

// M5/6/8 bolts
   boltd = (m==5)?(5.50+_tol):((m==6)?6.60+_tol:8.80+_tol);

  color(_zaxisColor) {
  difference(){
    zplatebase(_prof,h);
 // extra bolthead spaces
    translate([prof/2,_ysize/2,-1]) {
      translate([ 0,(_ysize-_boltspacing/2)/2, _zsize-1])  washerHole(size = m,length=5,tolerance=_tol);
      translate([ 0,-(_ysize-_boltspacing/2)/2, _zsize-1]) washerHole(size = m,length=5,tolerance=_tol);
    }
  }
  }
  
  if (_showBolts==1){
    translate([prof/2,_ysize/2,-1]) {
      translate([ 0, (_ysize-_boltspacing/2)/2, _zsize]) rotate([0,180,0]) socketBolt(size = m,length=12,tolerance=_tol);
      translate([ 0,-(_ysize-_boltspacing/2)/2, _zsize]) rotate([0,180,0]) socketBolt(size = m,length=12,tolerance=_tol);
    }
    
     translate([prof+_moc+1,_ysize/2,-1]){
        
        translate([ 15.5, 15.5,_zsize+1]) rotate([0,180,0]) socketBolt(size = 3,length=12,tolerance=_tol);
        translate([-15.5,-15.5,_zsize+1]) rotate([0,180,0]) socketBolt(size = 3,length=12,tolerance=_tol);
        translate([ 15.5,-15.5,_zsize+1]) rotate([0,180,0]) socketBolt(size = 3,length=12,tolerance=_tol);
        translate([-15.5, 15.5,_zsize+1]) rotate([0,180,0]) socketBolt(size = 3,length=12,tolerance=_tol);
        
    }
  }


}


module zplatetop(prof=_prof, h=6, m=5){
 
  _xsize    = 2*(_moc) + prof + 1; // mm
  _ysize    = 2*(_moc) + 18; // mm
  _zsize    =   h   ; // mm

   mirror([0,0,1])
   difference(){

  color(_zaxisColor) union(){
     hull(){
     translate([prof/2,_ysize/2,_zsize/2]) roundedBox([prof,_ysize,_zsize],_corner,true);
     translate([prof+_moc+1,_ysize/2,0]) cylinder(d=_bearing_diameter+10,h=_zsize);
     }
     translate([prof,0,0]) hull(){translate([0,h/2+2,0])sphere(d=h);translate([0,_ysize-h/2-2,0])sphere(d=h);}
  }

  translate([0,-0.5,-h]) cube([prof+_tol,_ysize+1,_zsize]);
  translate([0,-0.5,-h-2]) cube([prof+5,_ysize+1,_zsize]);

  color("orange") translate([prof+_moc+1,_ysize/2,-1]) {
    translate([0,0,0]) cylinder(d=_leadscrew_diam+1+_tol*2,h=_zsize+10.0);
    translate([0,0,0]) cylinder(d=_bearing_diameter+_tol*2,h=_zsize-2);
    translate([_straightrod_offsetx,_straightrod_offsety,0]) cylinder(d=_straightrod_diam+_tol, h=_zsize+2);
  }


  color("orange") {translate([prof/2,_ysize/2,-1]) {
      translate([ 0,(_ysize-_boltspacing/2)/2,-1])   boltHole(size = m,length=_zsize+3,tolerance=_tol);
   //   translate([ 0,(_ysize-_boltspacing/2)/2, 7])   washerHole(size = m,length=5,tolerance=_tol);
      translate([ 0,-(_ysize-_boltspacing/2)/2,-1])  boltHole(size = m,length=_zsize+3,tolerance=_tol);
   //   translate([ 0,-(_ysize-_boltspacing/2)/2, 7])  washerHole(size = m,length=5,tolerance=_tol);
    }
  }

  }
  
  if (_showBolts==1){
    translate([prof/2,_ysize/2,-1]) {
      translate([ 0,(_ysize-_boltspacing/2)/2,-5])   socketBolt(size = m,length=12,tolerance=_tol);
      translate([ 0,-(_ysize-_boltspacing/2)/2,-5])  socketBolt(size = m,length=12,tolerance=_tol);
    }
  }

}

