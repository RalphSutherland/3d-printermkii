//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>
use <std/skxx.scad>
use <std/scxuu.scad>
use <std/chain.scad>
use <y/yplatecorner.scad>
use <y/ymounts.scad>
use <y/yclamp.scad>
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

//======================================================================
// main program, comment components as needed
//======================================================================

yaxis(ypos=350);
//yrods(width=556,prof=30,length=600,d=_ydiam);

//======================================================================
// modules
//======================================================================

module yaxis(ypos=150, cables = 1){
  h = skheight(_ydiam)+1;
  dh = h-(skbelowc(_ydiam)+scbelowc(_ydiam));
  dz=h;

//echo(h,dh);
    translate([0,-_platesize/2+ypos+5,0]){
        translate([0,0,dz]) ycarriage(width=_platesize);
        translate([0,0,dz-1]) ysupports(width=_platesize);
        ybearings(d=_ydiam);
        color(_yaxisColor) translate([0,0,dz-0.5]) cube([240,240,1],center=true);
        color(_yaxisColor) translate([0,0,dz-yclampheight()-1]) yclamp();
    }

    yrods(width=_insideWidth, prof=_prof, length=_yrods, sep=_rodspacingx, d=_ydiam);

    translate([0,-w/2, 0]){
        ymotor(length=60.0);
        //belt approx
        color([0.2,0.2,0.2,1.0])translate([0,_insideWidth/2,-8+_h])cube([6,_insideWidth,2],center=true);
        color([0.2,0.2,0.2,1.0])translate([0,_insideWidth/2,-21+_h])cube([6,_insideWidth,2],center=true);

    }

    translate([0,w/2, 0]){
        yidle(d=18);
    }


    if (cables == 1){

    translate([-_platesize/2+6,-_platesize/2+ypos+5,dz-6])
    rotate([180,0,90]){
    if (ypos==360){
    chain([0,1,1,0,1,1,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,]);
    }
    if (ypos==350){
    chain([0,0,1,1,0,1,1,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==175){
    chain([0,0,0,0,0,0,0,0,1,1,0,1,1,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==150){
    chain([0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,0,
           0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==100){
    chain([0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,
           0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==50){
    chain([0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,
           0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==0){
    chain([0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==-10){
    chain([0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    }
    }// cables

}

// plate
module ycarriage(width=_platesize){
color("silver")translate([0,0,25+1.5])cube([width+10,width+10,3],center=true);
translate([0,0,28+1.5])cube([width,width,3],center=true);
color("DimGrey")translate([0,0,1.5])cube([width+10,width+10,3],center=true);
}

module ysupports(width=_platesize) {

    w     = width+10;
    sep   = w-45; // mm
    sh    = sep/2;
    hg    = 50;
    diam  = 5;
    //echo("ysupports sep",sep);

    color("yellow"){
        translate([-sh,-sh,-16]) cylinder(d=diam,h=hg);
        translate([-sh, sh,-16]) cylinder(d=diam,h=hg);
        translate([ sh, sh,-16]) cylinder(d=diam,h=hg);
        translate([ sh,-sh,-16]) cylinder(d=diam,h=hg);
    }

    color("white"){
        translate([-w/2,-w/2,0]) yplateCorner();
        translate([-w/2, w/2,0]) rotate([0,0,-90]) yplateCorner();
        translate([ w/2, w/2,0]) rotate([0,0,180]) yplateCorner();
        translate([ w/2,-w/2,0]) rotate([0,0, 90]) yplateCorner();
    }

}

// 4 bearings
module ybearings(sep=_rodspacingx, d=_ydiam) {
    shx    = sep/2;
    shy    = shx-sclength(_ydiam)/2-10;
    translate([0,0,skbelowc(d)+scabovec(d)]){
         translate([-shx,-shy,0]) rotate([0,180,0]) scxxuu(d);
         translate([-shx, shy,0]) rotate([0,180,0]) scxxuu(d);
         translate([ shx, shy,0]) rotate([0,180,0]) scxxuu(d);
         translate([ shx,-shy,0]) rotate([0,180,0]) scxxuu(d);
    }
}

module yrods(width=556, prof=30, length=500, sep=_rodspacingx, d=_ydiam){
     h    = prof/2;
     w    = width;
     l    = length;
    sh    = sep/2;
    tol   = 0.1;
//    echo("wy",w);
//    echo("ly",l);
// yaxis rods and supports
    color("silver") translate([-sh,(l+tol)/2,20]) rotate([90,0,0]) cylinder(d=d, h=l+tol);
    translate([-sh,-w/2-h,20]) {
        color("Gray")skxx(d);
        color("Silver")translate([16,0,-14])  rotate([0,180,0]) socketBolt(size=5,length=12);
        color("Silver")translate([-16,0,-14]) rotate([0,180,0]) socketBolt(size=5,length=12);
    }
    translate([-sh, w/2+h,20]) {
        color("Gray")skxx(d);
        color("Silver")translate([16,0,-14])  rotate([0,180,0]) socketBolt(size=5,length=12);
        color("Silver")translate([-16,0,-14]) rotate([0,180,0]) socketBolt(size=5,length=12);

    }

    color("silver")translate([+sh,(l+tol)/2,20]) rotate([90,0,0]) cylinder(d=d, h=l+tol);
     translate([+sh,-w/2-h,20]) {
         color("Gray")skxx(d);
        color("Silver")translate([16,0,-14])  rotate([0,180,0]) socketBolt(size=5,length=12);
        color("Silver")translate([-16,0,-14]) rotate([0,180,0]) socketBolt(size=5,length=12);
     }
     translate([+sh, w/2+h,20]){
         color("Gray")skxx(d);
        color("Silver")translate([16,0,-14])  rotate([0,180,0]) socketBolt(size=5,length=12);
        color("Silver")translate([-16,0,-14]) rotate([0,180,0]) socketBolt(size=5,length=12);
     }


}
