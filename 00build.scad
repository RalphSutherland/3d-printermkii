//======================================================================
//
// Aluminium 3030 Profile Box Frame
// Cartesian Prusa i3 style
// Large print volume, 340x340x240mm
// RSS 2016-2017
//
//======================================================================
//======================================================================
// Uses
//======================================================================

use <frame.scad>
use <xaxis.scad>
use <yaxis.scad>
use <zaxis.scad>
//use <wiring/panel.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

// print head location mockup

_xpos =  100;
_ypos =  100;
_zpos =  50;

_ShowHotend   = 1 ; // 1 or 0
_ShowCables   = 1 ; // 1 or 0
_ShowSwitches = 1 ; // 1 or 0

//======================================================================
// main program, comment components as needed
//======================================================================

translate([0,0,_prof]){
   translate([0,0,0]) frame();
   translate([0,0,0]) feet();
  // translate([0,0,0]) sheets();
   translate([0,0,0]) zdrive();
   translate([0,0,0]) ydrive();
   translate([0,0,0]) xdrive();
//   translate([0,0,0]) panels();
}

//======================================================================
// modules
//======================================================================

module xdrive(standoffy = _yoffxz, car=0){

    translate([0,standoffy-(32+_moc),120+_zpos]) {

        xaxis(length = _xrods, endsep=_xendLength, barsep=45,
              posx   = _xpos, posz = _zpos,
              depth  = _rodoffdepth, car=_ShowHotend, cables=_ShowCables);

/*
        echo("length:",_xrods);
        echo("endsep:",_xendLength);
        echo("barsep:",45);
        echo("posx:",_xpos);
        echo("posz:",_zpos);
        echo("depth:",_rodoffdepth);
*/
      
    }
}

module ydrive(){
    yaxis(ypos=_ypos,cables=_ShowCables);
}

module zdrive(standoffy = _yoffxz){
     zaxis(standoffy); // no zpos, offset applied to x axis
}

module panels(){
   color("White"){
       translate([0,-w/2-_prof,0]) {
       translate([-30,0,0]) rotate([90,180,0]) 
           panel(n=4, d=[18,14,8,16],t=3,s=["B","BT","O","H"],flip=1);
       translate([160,0,0]) rotate([90,180,0]) 
           panel(n=5, d=[16,16,16,16,16],t=3,s=["X","Y","Z1","Z2","E1"],flip=1);
       }
   if (_ShowSwitches==1){
       translate([l/2-_h,-_yoff,40]){
       zendstop();
       }
   }
   }
}
