//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/fasteners.scad>
use <std/supports.scad>

//======================================================================
// Parameters all dimensions in mm
//======================================================================

    $fn=60;

    _tol=0.1;
    _ShowFittings = 0;
    bolt_y_centres = 24.;//24.25,23,24
    bolt_x_centres = 24.;//24.25,23,24

    radiator_diameter  = 22.0;// E3Dv6 22, CHX 24.75
    radiator_top       = 42.75;// E3Dv6 42.75, CHX 50.0
    radiator_fittingHeight = 12.75;

    extruder_height=radiator_top-0.25;//42.5 to top of bracket, less small extend
//======================================================================
// main program, comment components as needed
//======================================================================

    E3Dv6_Mount();
//      translate([-25,0,24])E3Dv6_MountBar();
//      rotate([0,-90,0]) E3Dv6_MountBack();

//======================================================================
// modules
//======================================================================

module E3Dv6_Mount(){
rotate([180,0,0])
  difference(){

    union(){
        plate();
        translate([0,0,10]){
            bracket();
            bracketBar();
            translate([8.0,0,16]) {
                 corner(c=[4.1,bolt_x_centres+10,4.1]);
            }
            translate([8.0,0,-2]) {
                 rotate([0,90,0]) corner(c=[4.1,bolt_x_centres+10,4.1]);
            }
        }
    }// union

    translate([0,0,5]){
        translate([-1,bolt_x_centres/2,bolt_y_centres]) {
             rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
            translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
        }
        translate([-1,bolt_x_centres/2,0]){
             rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
            translate([6,0,0]) rotate([0,90,0])  washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
        }
        translate([-1,-bolt_x_centres/2,bolt_y_centres]) {
             rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
            translate([6,0,0]) rotate([0,90,0])  washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
        }
        translate([-1,-bolt_x_centres/2,0]) {
             rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
            translate([6,0,0]) rotate([0,90,0])  washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
        }
    }

   }// diff

}


module E3Dv6_MountBack(boltholes=1){
rotate([180,0,0])
  difference(){

    union(){
       plate();
        translate([0,0,10]){
            bracket(boltholes);
            translate([8.0,0,16]) {
                 corner(c=[4.1,bolt_x_centres+10,4.1]);
            }
            translate([8.0,0,-2]) {
                 rotate([0,90,0]) corner(c=[4.1,bolt_x_centres+10,4.1]);
            }
        }
    }// union

      if (boltholes==0){
          translate([0,0,5]){
              translate([-1,bolt_x_centres/2,bolt_y_centres]) {
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
              translate([-1,bolt_x_centres/2,0]){
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
              translate([-1,-bolt_x_centres/2,bolt_y_centres]) {
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
              translate([-1,-bolt_x_centres/2,0]) {
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
          }
      }

   }// diff

}


module E3Dv6_MountBar(){
    rotate([180,0,0]) translate([0,0,10])
       bracketBar();
}

module E3Dv6_MountRing(){
    rotate([180,0,0]) translate([0,0,10])
       bracketRing();
}

module E3Dv6_MountRetainer(){
    rotate([180,0,0]) translate([0,0,10])
       bracketRetainer();
}

module corner(c=[1,1,1]){
    difference(){
         cube(c,true);
         translate([c[0]/2,(c[1]+1)/2,c[2]/2]) rotate([90,0,0]) cylinder(r=c[0],h=(c[1]+1));
    }
}

module plate(){

    ph=bolt_y_centres+12;
    pw=bolt_x_centres+12;
    pt=6.0;
    translate([pt/2,0,ph/2])
    rotate([0,90,0]){
        roundedBox([ph,pw,pt],3,true);
    }
}

module _bracket(boltholes=1, tabs=1, highHoles=0){

    d=16.0;
    h=radiator_fittingHeight+1.25-_tol;
    l=20.0;
    l2=16.0;
    pw=bolt_x_centres+12;
    ph=bolt_y_centres+12;
    pt=6;
    slice_thick = 0.25;

    difference(){

        union(){
           translate([   l/2, 0,h/2]) cube([l,l,h],true);
           translate([     l, 0,  0]) rotate([0,0,22.5]){
           minkowski(){ // smoothed off hex
               cylinder(d=l+4-3,h=h-1, $fn=8);
               cylinder(d=3,h=1);
               }
            }
           hull(){
              if (tabs==1) {
                  translate([ l, -13,h/2]) rotate([ 0, 0, 90])rotate([90, 0, 0])
                      roundedBox([d-1.5,h,10],3,true);
                  translate([ l,  13,h/2]) rotate([ 0, 0, 90])rotate([90, 0, 0])
                      roundedBox([d-1.5,h,10],3,true);
              }
              translate([ pt/2, 0,h/2]) rotate([0,90,0])
                  roundedBox([h,pw,pt],3,true);
            }// hull
        }//union

        if (tabs==1) translate([l,0,(h/2)]) cube([slice_thick,50,h+2],true);

        translate([ l, 0,-1.3]) cylinder(d=l2+0.70,h=h+2);
        translate([ l, 0,-1.5]) cylinder(d=12.5,h=h+2);

        if (highHoles ==0){
              if (boltholes==1){
                     translate([0.7,   15.5,h/2])rotate([0,90,0]) {
                          translate([0,0,pt])boltHole(4,length=20,tolerance=0.1);
                         hull(){
                              translate([0,0,l/2+0.9]) nutHole(4,tolerance=0.2);
                              translate([0,7,l/2+0.9]) nutHole(4,tolerance=0.2);
                              translate([0,0,l/2+0.6]) nutHole(4,tolerance=0.2);
                              translate([0,7,l/2+0.6]) nutHole(4,tolerance=0.2);
                         }
                     }
                     translate([0.7,  -(15.5),h/2])rotate([0,90,0]) {
                          translate([0,0,pt])boltHole(4,length=20,tolerance=0.1);
                         hull(){
                              translate([0, 0,l/2+0.9]) nutHole(4,tolerance=0.2);
                              translate([0,-7,l/2+0.9]) nutHole(4,tolerance=0.2);
                              translate([0, 0,l/2+0.6]) nutHole(4,tolerance=0.2);
                              translate([0,-7,l/2+0.6]) nutHole(4,tolerance=0.2);
                         }
                     }
              }
       } else { // highHoles == 1
                translate([11,   15.5,h/2-4.5])rotate([0,90,0])rotate([0,0,30])
                   cylinder(d=2.9,h=15,$fn=6);
                translate([11,  -15.5,h/2-4.5])rotate([0,90,0])rotate([0,0,30])
                   cylinder(d=2.9,h=15,$fn=6);
        }


    }//diff

    difference(){
        translate([l,0,3.60])cylinder(d=l2+0.75,h=5.6);
        translate([l,0,3.30])cylinder(d=12.5,h=6.8);
        if (tabs==1) translate([l,0,(h/2)])cube([slice_thick,50,h+2],true);
    }

        if (highHoles ==0){
           if (boltholes==1){
                 translate([0.70+l/2+0.58,  -19,h/2+3.5])rotate([0,90,0])
                      supports(c=[7,7,3.8],dir=0,dx=3);

                 translate([0.70+l/2+0.58,  19-7,h/2+3.5])rotate([0,90,0])
                      supports(c=[7,7,3.8],dir=0,dx=3);
           }
        }

}

module bracket(boltholes=1){
     d=16.0;
     h=radiator_fittingHeight+1.25-_tol;
     l=20.0;
    l2=16.0;

    intersection(){
       _bracket(boltholes);
       translate([(l/2),0,(h/2)])cube([l,50,h+2],true);
     }
}

module bracketBar(boltholes=1){
     d=16.0;
     h=radiator_fittingHeight+1.25-_tol;
     l=20.0;
    l2=16.0;

    intersection(){
       _bracket(boltholes);
       translate([l+10,0,(h/2)])cube([20,50,h+2],true);
     }
}


module bracketRing(){
     d=16.0;
     h=radiator_fittingHeight+1.25-_tol;
     l=20.0;
    l2=16.0;

    intersection(){
       _bracket(boltholes=0, tabs=0);
       translate([l+10,0,(h/2)])cube([20,50,h+2],true);
     }

}

module bracketRetainer(){
     d=16.0;
     h=radiator_fittingHeight+1.25-_tol;
     l=20.0;
    l2=16.0;

    intersection(){
       _bracket(boltholes=0, tabs=1, highHoles=1);
       translate([l+10,0,(h/2)])cube([20,50,h+2],true);
     }

}

module brace(){
  linear_extrude(height=5){
      polygon([[0,0],[10,0],[0,10]]);
  }
}
