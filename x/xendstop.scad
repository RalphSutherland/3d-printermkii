//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================
include <../const.scad>
//======================================================================

rod=_xdiam; // 8 mm
beam=_rodsep;//44.5 mm

thick=2; //mm
width=10; //mm
cyl_thick=2; //mm

// switch holes:
sw_width=9.5; //mm
sw_height=3.0; //mm
sw_bolt =2; // M2/1.6 fitting

difference(){ // diff0
union(){     // union1

difference(){ // diff1
    
 union(){   
     translate([0,thick+rod*0.5,0]) cylinder(r=(0.5*rod+cyl_thick),h=width);
     translate([0,(rod+thick)*0.5,0]) cylinder(r=(0.5*rod+cyl_thick),h=width);
 }
 
 translate([0,thick+rod*0.5,-1]) cylinder(r=rod/2,h=width+2);
} // diff1

difference(){ // diff2
    union(){   
         translate([beam,thick+rod*0.5,0]) cylinder(r=(0.5*rod+cyl_thick),h=width);
         translate([beam,(rod+thick)*0.5,0]) cylinder(r=(0.5*rod+cyl_thick),h=width);
     }
     translate([beam,thick+rod*0.5,-1]) cylinder(r=rod/2,h=width+2);
} // diff2

    translate([0,-thick*0.5,0])  cube([beam,thick,width]);
} // union1

translate([-rod*0.5-cyl_thick,rod,-1])  cube([beam+(rod+cyl_thick*2),thick*4,width+2]);

color("red") {
hull(){
    translate([rod+cyl_thick-0.25,thick,sw_height])  rotate([90,0,0]) boltHole(sw_bolt,length=thick*2);
    translate([rod+cyl_thick-0.25,thick,sw_height])  rotate([90,0,0]) boltHole(sw_bolt,length=thick*2);
}
hull(){
    translate([rod+cyl_thick-0.25+sw_width,thick,sw_height])  rotate([90,0,0]) boltHole(sw_bolt,length=thick*2);
    translate([rod+cyl_thick-0.25+sw_width,thick,sw_height])  rotate([90,0,0]) boltHole(sw_bolt,length=thick*2);
}
} // red
}