use <std/boxes.scad>;
use <std/fasteners.scad>;
use <std/skxx.scad>;
include <../const.scad>

_size = 10;// skxx rod size
_mount_depth = 15.0;//mm
_mount_thick = 6.0;//mm

_sw_depth = 7.0; // mm switch hole from switch edge
_sw_width = 9.5; // mm switch hole separation
_sw_msize = 2; // hole size, M2, M3 etc
_sw_len  = 10;// mounting bolt length mm

difference(){
union(){

//locking stand
  
hull(){
translate([0,0,0.5])
     cube([skwidth(_size ),_mount_thick,_mount_thick]);
  
translate([skwidth(_size )/2,_mount_thick,skheight(_size)/2]) 
     rotate([90,0,0]) 
         cylinder(d=skheight(_size )-5,h=_mount_thick);
}

// sleave and mounting point
//hull(){
translate([skwidth(_size)/2,_mount_thick,skbelowc(_size)]) rotate([90,0,0]) {
     //cylinder(d=_size+6,h=_mount_depth+_mount_thick);
     translate([0,0,_mount_thick+_mount_depth/2])
         rotate([0,0,0])
         roundedBox([_size+6,_size+6,_mount_depth],2,true);
}
//}

}//u

// loose shaft
translate([skwidth(_size)/2,_mount_thick+1,skbelowc(_size)]) 
    rotate([90,0,0]) 
        cylinder(d=_size+0.5,h=_mount_depth+_mount_thick+2);

// mounting holes
   translate([skwidth(_size)/2,-_mount_depth+_sw_depth,skbelowc(_size)]){
        translate([ _sw_len-_sw_len/2,0, _sw_width/2]) 
             rotate([0,90,0])
                 boltHole(size=_sw_msize,length=_sw_len,$fn=6);
        translate([ _sw_len-_sw_len/2,0,-_sw_width/2]) 
             rotate([0,90,0])
                 boltHole(size=_sw_msize,length=_sw_len,$fn=6);
   }

   translate([skwidth(_size)/2,-_mount_depth+_sw_depth,skbelowc(_size)]){
        translate([ -_sw_len-_sw_len/2,0, _sw_width/2]) 
             rotate([0,90,0])
                 boltHole(size=_sw_msize,length=_sw_len,$fn=6);
        translate([ -_sw_len-_sw_len/2,0,-_sw_width/2]) 
             rotate([0,90,0])
                 boltHole(size=_sw_msize,length=_sw_len,$fn=6);
   }

}

