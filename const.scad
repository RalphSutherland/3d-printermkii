//======================================================================

$fn=60;

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

_pi=3.14159265358979;

_tol       = 0.1;
_prof      = 30 ;// profile size mm
_profile   = 30; // Al profile width for bracket, 20,30 or 40, adjust bolt hole sizes as needed
_h    = _profile/2;
_mod  =   42.3; // nema 17 motor width
_moc  = _mod/2; // motor center radius
_modx =   31.0; // motor bolt spacing
_base = 20.0 ; // mm 30 for prof = 20, 20 for 30

_xrods = 500.0; //mm
_xdiam = 8.0 ; //mm rod diameter
_yrods = 600.0; //mm
_ydiam = 10.0 ; //mm rod diameter
_zrods = 400.0; //mm
_zleads = 400.0; // leadscrew length
_zdiam = 10.0 ; //mm rod diameter

_platesize = 340.0;  //mm

_yoff   = 36; // mm offset of support
_yoffxz = _yoff-6; // mm offset of x-z axis components
_insideLength = _platesize+85; // mm x
_insideWidth  = _yrods-_prof-14; // mm y 600-prof-14(sk10) -
_insideHeight = _zrods; // mm z
_topWidth     = 320; // mm
_basespace    = 100;// mm

_rodoffsetx   = 28.0;//mm
_rodoffsety   = 0.0;//mm
_rodoffsetz   = 0.0;
_rodsep       = 45.0; // rods center-center vertical
_rodspacingx  = 200;
_rodlr2       = 11.0; // lead
_rodbr2       = 13.5; /// 11.5 LM8 13.5 LM10
_rodoffdepth  = 30.0; // 0.5*(_xrods-_insideLength)-_prof-_moc-1+_rodoffsetx+_rodbr2;
//_xendLength  = (_insideLength + 2*_prof)+2*(_moc+1-_rodlr2);// mm from xend to xend
//mm into x blocks rel to straight rod mount
_extensionLength = 17;
_xendLength  = (_insideLength + 2*_prof)+2*(_moc+1-_rodbr2-_rodoffsetx);// mm from xend to xend

_carriageBoltSpacing = 24;
_carriage_width      = 56;
_carriage_height     = 68;
_carriage_axis_depth = 7.75+4-1;// see xcarriage.scad axis_bearing_z

l = _insideLength + 2*_prof;
w = _insideWidth;
hg = _insideHeight;

// Colors!
_footColor = [0.8,0.2,0.2,1.0];
_fitColor  = "White";
_fitClearColor = [1.0,1.0,1.0,0.6];

_xaxisColor  = "White";
_yaxisColor  = "White";
_zaxisColor  = "White";

//======================================================================
