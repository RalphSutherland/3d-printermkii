use <std/boxes.scad>;
use <std/fasteners.scad>;
$fn = 30;
//
tNutP30M5();
//
module tNutP30M5(){

    _msize    =   5; // M5
    _tol      = 0.1; // for holes, mm

    dx  =  8; // mm P3030 slot width 8.2mm
    dy  = 16; // mm P3030 inside max width 16.5mm
    dy2 = 10; // mm
    dz  =  2; // mm

    difference(){
        hull(){ // 6mm tall
            translate([0,0,dz/2])   tBox([dy,dx,dz],radius=1,ratio=4);
            translate([0,0,dz*2+dz/2]) roundedBox([dy2,dx,dz],2,true);
        }
        translate([0,0,  dz])  nutHole(size=_msize,tolerance=_tol);
        translate([0,0,-0.1]) boltHole(size=_msize,tolerance=_tol);
    }

}
