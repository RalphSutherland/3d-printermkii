use <std/boxes.scad>;
use <std/fasteners.scad>;

$fn=90;

slotnutP30M6();

module slotnutP30M6(){

difference(){
hull(){
translate([0,0,1])roundedBox([15.5,15.5,2],2,true);
translate([0,0,5])roundedBox([15.5,9.5,2],2,true);
}

translate([0,0,2])nutHole(size=6,tolerance=0.1);
translate([0,0,-1])boltHole(size=6);

}

}