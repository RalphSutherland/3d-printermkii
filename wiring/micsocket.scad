//======================================================================
// Uses
//======================================================================
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

$fn=60;

//======================================================================
// main program, comment components as needed
//======================================================================

translate([0,0,32])
    rotate([0,180,0]) {
        micSocketCover( split=0,slot=0 );
       // micSocketCover("X",label=1,split=1,slot=1,font="Comfortaa:style=Bold",size=8);
    }

//======================================================================
// modules
//======================================================================


module micSocketCover(t="",label=0, split=0, slot=0, font="DejaVu Sans:style=Bold",size=10) {
    
    difference(){
        
     union(){
        hull(){
             cylinder(d=24,h=12);
             cylinder(d=16,h=32);
        }
        hull(){
             cylinder(d=16,h=32);
             translate([0,9.8,26]){
                 cube([12,2,12],true);
              } //trans
        }// hull
    }//union
    
    if (label == 1){
             translate([0,10.5,30]){
                 rotate([-90,0,0])
                     linear_extrude(height=2)
                         text(t,font=font,size=size,halign="center");
             }
         }
     
    translate([0,0,-0.5]) {
        cylinder(d=22.22,h=6.5,$fn=6);
        cylinder(d=16,h=12.5);
        cylinder(d=12,h=25);
        if (slot==1){
            translate([-3,-1.5,0]) cube([6,3,33]);
        }
        if (slot==0){
            cylinder(d=8,h=33);
        }
    }
    
    if (split == 1){
        translate([0,-0.75,-0.5]) cube([13,1.5,33]);
    }
    }//diff
    
}