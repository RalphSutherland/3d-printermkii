//======================================================================
// Uses
//======================================================================
use <std/chain.scad>
use <std/motors.scad>
use <std/GT2Gear.scad>
use <std/lmxuu.scad>
//
use <x/xmounts.scad>
use <hotend.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

//======================================================================
// main program, comment components as needed
//======================================================================

xaxis(length=500, endsep=446.3, barsep=_rodsep,
      depth=30, posx = 0, posz=100,
      car=1 , cables = 0 );

//======================================================================
// modules
//======================================================================

module xaxis(length=500, endsep=420, barsep=_rodsep, depth=30,
    posx = 0, posz = 0, car = 0, cables = 0){

//  echo(length,endsep,depth,(length-(endsep))-depth+10.5);

     dzr =_rodoffsetz;
     barsep=_rodsep;
     lm=_zdiam;

     dxrods = 3.5+lm;//_lr2+_rodoffsetx lm8 11.5, lm10 13.5
     dyrods = 7+lm;//// lm8 15, lm10 17
     dzrods = 26-lm;//// lm8 18, lm10 20

     translate([endsep/2,0,0]) {
         xmotorend();
         translate([ dxrods,dyrods, dzrods])rotate([90,0,0])lmxuu(lm);
         translate([ dxrods,dyrods,-dzrods])rotate([90,0,0])lmxuu(lm);
     }
     translate([-endsep/2,0,0]) {
         xidleend();
         translate([-dxrods,dyrods, dzrods])rotate([90,0,0])lmxuu(lm);
         translate([-dxrods,dyrods,-dzrods])rotate([90,0,0])lmxuu(lm);
     }

     rn = 12.73239544735163;

     translate([0,0,0]){

          color("black")
          difference(){
          hull(){
              translate([-endsep/2+10.5-4,-3,0])rotate([-90,0,0]){
              cylinder(d=rn+1,h=6);
               }
               translate([endsep/2+73,-3,0])rotate([-90,0,0]){
               cylinder(d=rn+1,h=6);
               }
          }

          hull(){
               translate([-endsep/2+17-4,-3,0])rotate([-90,0,0]){
               //GT2Nidle(20);
               cylinder(d=rn-0.5,h=7);
               }
               translate([endsep/2+73,-3.5,0])rotate([-90,0,0]){
               //GT2Nidle(20);
               cylinder(d=rn-0.5,h=7);
               }
          }
          }// diff

     }//trans

     if( car == 1){
     // +5 for E3Dv6
          translate([(_platesize/2)-posx+5,10.5,-34]) rotate([0,0,90])
               hotend(rods=barsep);
     }

     color("silver")translate([(endsep)/2+depth,0,-(barsep/2)-dzr]) rotate([0,-90,0])
          cylinder(d=8,h=length, $fn=30);
     color("silver")translate([(endsep)/2+depth,0,(barsep/2)+dzr]) rotate([0,-90,0])
          cylinder(d=8,h=length, $fn=30);



        if (cables == 1) {

        translate([endsep/2,0,0]) {
       if (posz==240){
       // vert
         translate([72,-50,-28]) rotate([0,90,180])
         chain([ 0,0,0,0,0,0,0,0,
                 0,0,0,1,0,1,1,1,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0]);

        }
       if (posz==200){
         // vert
         translate([72,-50,-28]) rotate([0,90,180]) {
         chain([ 0,0,0,0,0,0,0,
                 0,0,0,1,0,1,1,1,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0]);
         }

         if (posx==100){
         // x dir
         translate([((_platesize-endsep)/2)-posx,12,50]) rotate([0,0,0])
         chain([ 0,0,0,0,1,0,1,1,0,0,0,
                 1,0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0]);
         }

         if (posx==0){
         // x dir
         translate([((_platesize-endsep)/2)-posx,12,50]) rotate([0,0,0])
         chain([ 0,1,1,1,0,0,0,0,
                 1,0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0]);
         }

        }
       if (posz==150){
         translate([72,-50,-28]) rotate([0,90,180])
         chain([ 0,0,0,0,0,0,0,0,1,0,1,1,1,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0]);
        }
        if (posz==100){
         translate([72,-50,-28]) rotate([0,90,180]) {
         chain([ 0,0,0,0,0,0,1,0,1,1,1,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0]);
          translate([-238.2,0,46.54]) rotate([180,0,180])chain_plate_big(1,1);
         }

         if (posx==100){
         translate([((_platesize-endsep)/2)-posx,12,50]) rotate([0,0,0])
         chain([ 0,0,0,1,0,1,1,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,1,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0]);
         }

         if (posx==0){
         translate([((_platesize-endsep)/2)-posx,12,50]) rotate([0,0,0])
         chain([ 0,1,1,1,0,0,0,0,
                 1,0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0]);
         }


        }
        if (posz==50){
         translate([72,-50,-28]) rotate([0,90,180])
         chain([ 0,0,0,0,1,0,1,1,1,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0]);
        }
        if (posz==0){
         translate([72,-50,-28]) rotate([0,90,180])
         chain([ 0,0,1,0,1,1,1,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0,0,0]);
        }

    }// show cables

    }



}
