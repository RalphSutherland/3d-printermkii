//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/fasteners.scad>
use <std/supports.scad>
use <std/motors.scad>
use <std/GT2Gear.scad>
use <xendbar.scad>
// v1.1.2

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <../const.scad>

// set printing  for 0.5mm wide x 0.2mm layers

_rod = _xdiam+2*_tol ; // mm x-rod diameter

_swapRodOrder = 0;

_lr1 = 0.5*10.2 ; // mm leadscrew housing inner radius
_lr2 = _rodlr2 ; // mm leadscrew housing outer radius
              // lm8 D1  = 15, br1=15/2 = 7.5mm
              // lm8 br2 = (15)/2+4mm walls = 11.5mm
              // lm10 D1  = 19, br1=19/2 = 9.5mm
              // lm10 br2 = (15)/2+4mm walls = 13.5mm
_br2 = _rodbr2 ; // mm bearing housing outer radius
_br1 =  _br2-4 ; // mm bearing housing inner radius
              // lm8  = br1+7.5 = 15
              // lm10 = br1+7.5 = 17
_ds  = _br1+7.5 ;// mm offset from belt center to shaft centers
_dls =  0.0 ;// mm offset from leadscrew to bearing shaft + in front
             //
_shaftoffsetx  = abs(_rodoffsetx);//28
_blockWidth    = 17.0;//mm
_fullHeight    = 60.0;//mm

// 0 point is at edge of bearing mounts, extensions are extras that wont move shaft coords
_baseLength = _lr2+_shaftoffsetx+_br2;//mm +_motorExtensionLength beyond 0
//
_motorExtensionLength = 10.5;
_motorBaseLength  = _baseLength+_mod+2*_tol;
_motorBlockLength = _motorExtensionLength+_motorBaseLength;
_motorInset = 22;
//
_idleExtensionLength = 10.5;
_idleBlockLength     = _idleExtensionLength +_baseLength;//mm
//
//echo(_baseLength,_motorExtensionLength,_motorBlockLength,_idleExtensionLength,_idleBlockLength);
//
_leadmountheight =  9.0;
//
_roddepth     = 30+_tol*2; // depth of rod holes
_slotheight   = 25.0;
_slotoffsetz  = 0.0;
_slotwidth    = 8.0;
_slotdepth    = _modx+1+_shaftoffsetx+_lr2; //32+22+11; // 65 = 32+22+11
//
//======================================================================
//
_showMotor      = 1;
_showSupports   = 0;
_showTensionArm = 1;
_showBolts      = 1;// and other hardware
//
_col=_xaxisColor;
//
//======================================================================
// main program, comment components as needed
//======================================================================
//
//xmotorend();
//
xidleend();
//
//idletensioner(d=18,h=8.5);//(d=18,h=11);
//
//======================================================================
// modules
//======================================================================
//
module xmotorend(swap=1){
    rotate([180,0,0])
    translate([0,0,-_fullHeight/2]){
        difference(){
            union(){
                xmotormount(m=_showMotor);
                color(_col) shaftmounts(dir=-1*swap);
            }
            shaftholes(dir=-1*swap);
            slot();
            rodsleft();


        }
       if(_showBolts==1){
           color("Silver")  leadShaftBolts(dir=-1*swap);
       }
        if (_showSupports==1) color(_col) xmotorsupports();
    }
}

module xidleend(swap=1){
    if (_showTensionArm==1){
        translate([ 5, 0, 0]){
            idletensioner(d=18,h=8.5);//(d=18,h=11);
        }

        translate([-(_lr2+_shaftoffsetx+_br2)-1,0,0]) rotate([90,0,-90])
            xendbarinplace(rods=_rodsep, slot = _slotheight , bolts = _showBolts);

     }
        rotate([180,0,0])
        translate([-_baseLength,0,-_fullHeight/2]){
            difference(){
                union(){
                    color(_col) xidlemount();
                    color(_col) shaftmounts(dir=swap);
                }
                shaftholes(dir=swap);
                slotidle();
                rodsright();
            }
            if(_showBolts==1){
                color("Silver")  leadShaftBolts(dir=swap);
            }
            if (_showSupports==1) xidlesupports();
        }

}
module shaftmounts(dir=1){
    translate([(_baseLength)*(1-dir)/2,0,0])
    mirror([(1-dir)/2,0,0]) {
        translate([_lr2  ,-_ds,0]) {
            hull(){
                translate([0,-_dls,0])cylinder(r=_lr2,h=_leadmountheight);
                translate([_shaftoffsetx,0,0])cylinder(r=_br2,h=_leadmountheight);
                translate([-_lr2,0,0])cube([2*_lr2,_br2,_leadmountheight]);
            }
        }
        translate([_lr2+_shaftoffsetx,-_ds,0]){
            union(){
                cylinder(r=_br2,h=_fullHeight);
                translate([-_br2,0,0])cube([2*_br2,_br2,_fullHeight]);
            }
        }
    }
}

module leadShaftBolts(dir=1){
    translate([(_baseLength)*(1-dir)/2,0,0])
    mirror([(1-dir)/2,0,0]) {
        translate([_lr2   ,-_ds-_dls,0]) {
            translate([-8, 0,0])socketBolt(size=3,length=12);
            translate([ 8, 0,0])socketBolt(size=3,length=12);
            translate([ 0,-8,0])socketBolt(size=3,length=12);
            if (_dls>=6) {
                translate([ 0, 8,0])socketBolt(size=3,length=12);
            }
        }
    }
}
module shaftholes(dir=1){
    translate([(_baseLength)*(1-dir)/2,0,0])
    mirror([(1-dir)/2,0,0]) {
        translate([_lr2   ,-_ds-_dls,0]) {
            translate([ 0, 0,-1])cylinder(r=_lr1+_tol,h=11);
            translate([-8, 0,-1])cylinder(d=3.25,h=11);
            translate([ 8, 0,-1])cylinder(d=3.25,h=11);
            translate([ 0,-8,-1])cylinder(d=3.25,h=11);
            if (_dls>=6) {
                translate([ 0, 8,-1])cylinder(d=3.25,h=11);
            }
        }
        slot=1.0;//mm
        translate([_lr2+_shaftoffsetx,-_ds,0]){
            translate([0,0,-1]) cylinder(r=_br1-2*_tol,h=_fullHeight+2);
            translate([0,0,-1])cylinder(r=_br1+_tol,h=_fullHeight);
            translate([_br1/2+5,-slot/2,((_fullHeight+2)/2)-1])cube([_br1,slot, _fullHeight+2],center=true );
        }
    }
}
module rodsright(){
    color("silver"){// rods
        rh = _roddepth+50;
        translate([-5,0,(_fullHeight-_rodsep)/2]) rotate([0,90,0]) {
            cylinder(d=_rod+_tol,h=rh);
            translate([-_rod/4    ,0,(rh)/2-_tol]) cube([(_rod+_tol)/2,_rod+_tol,_roddepth+50],center=true);
            translate([-_rod/2-2.5,0,(rh)/2])cube([1,_rod+_tol,_roddepth+50],center=true);
        }
        translate([-5,0,_fullHeight-(_fullHeight-_rodsep)/2]) rotate([0,90,0]) {
            translate([_rod/4    ,0,(rh)/2+_tol])cube([(_rod+_tol)/2,_rod+_tol,(_roddepth+50)],center=true);
            translate([_rod/2+2.5,0,(rh)/2])cube([1,_rod+_tol,(rh)],center=true);            cylinder(d=_rod+_tol,h=(rh));
        }
    }
}
module rodsleft(){
    color("silver"){// rods
        rh = _roddepth+50;
        translate([-50,0,(_fullHeight-_rodsep)/2]) rotate([0,90,0]) {
            cylinder(d=_rod+_tol,h=rh);
            translate([-_rod/4,0,(rh)/2])cube([(_rod+_tol)/2,_rod+_tol,_roddepth+50],center=true);
            translate([-_rod/2-2.5,0,(rh)/2])cube([1,_rod+_tol,_roddepth+50],center=true);
        }
        translate([-50,0,_fullHeight-(_fullHeight-_rodsep)/2]) rotate([0,90,0]) {
            translate([_rod/4,0,(rh)/2])cube([(_rod+_tol)/2,_rod+_tol,(_roddepth+50)],center=true);
            translate([_rod/2+2.5,0,(rh)/2])cube([1,_rod+_tol,(rh)],center=true);
            cylinder(d=_rod+_tol,h=(rh));
        }
    }
}
module slot(){
    color("Orange")
    difference(){
        translate([-32,-_slotwidth/2,(_fullHeight-_slotheight)/2-_slotoffsetz])
        cube([_slotdepth+30,_slotwidth+_tol,_slotheight+_slotoffsetz+_tol]);
        translate([_motorBlockLength-_motorInset,_blockWidth,_fullHeight/2-_slotoffsetz]){
            rotate([90,0,0]){
                for(j=[-1:2:1]){
                    //for(i=[-1:2:1])
                    i = -1;
                  {
                    translate([i*_modx*0.5, j*_modx*0.5,10]){
                        hull(){
                            cylinder(d=12,h=14);
                            translate([0,0,7]) cube([14.5,4,14],center=true);
                        }
                    }
                  }
                }
            } //rot
        } //trans
    } // diff
}
module slotidle(){
    color([0.2,0.2,0.2,1.0])
    difference(){
        translate([-2,-_slotwidth/2,(_fullHeight-_slotheight)/2])
        cube([_slotdepth,_slotwidth+_tol,_slotheight+_tol]);
    } // diff
    // cutout for tension arm head block
    dcx = -4;
    translate([_idleBlockLength+dcx,0,_fullHeight/2])
      bowedBox([_modx-3,_blockWidth+3, _slotheight],f=0.3);

}
//bowedBox([40,20,40],0.5);
module bowedBox(c=[1,1,1],f=0.1){
    $fn=90;
    d = [c[0]*(1-f),c[1],c[2]];
    hull(){
        cube(d,center=true);
        translate([0,c[1]/2,0])rotate([90,0,0])cylinder(d=c[2],h=c[1]);
    }
}
module xmotormount(m=_showMotor){

    if (m==1){
        translate([_motorBlockLength-_motorInset-_motorExtensionLength,_blockWidth-8,_fullHeight/2-_slotoffsetz]){
            translate([0,48,0]){
                rotate([90,0,0])rotate([0,0,90]){
                    stepper_motor(nema_standard=17,length=48);
                    color("Silver")translate([0,0,56.5])rotate([180,0,0])
                        GT2N(20);
                }
            }
        }
    }//_showMotor

    color(_col){
    difference(){

        // main body
        union(){
           translate([_motorBlockLength/2 - _motorExtensionLength , 0,  _fullHeight/2])
                roundedBox([_motorBlockLength, _blockWidth,_fullHeight],3,true);
        }

        // motor mounting hole
        translate([_motorBaseLength-_motorInset,0,_fullHeight/2-_slotoffsetz]){
            bowedBox([_modx-2,_blockWidth+1, _modx-2],f=0.3);
            // rotate([90,0,0])rotate([0,0,22.5])cylinder(d=_modx+3,h=_blockWidth+2,$fn=8);
        }

        // motor mounting bolt holes
        translate([_motorBaseLength-_motorInset,_blockWidth,_fullHeight/2-_slotoffsetz]){
            rotate([90,0,0]){
                for(j=[-1:2:1]){
                    for(i=[-1:2:1]){
                        translate([i*_modx*0.5, j*_modx*0.5, 0])cylinder(d=3.5,h=30);
                        translate([i*_modx*0.5, j*_modx*0.5,24])cylinder(d=6.4,h=10);
                    }//i
                }//j
            }
        }

    }//diff
    } // col

   if(_showBolts==1){
       color("Silver"){
        translate([_motorBaseLength-_motorInset,-8,_fullHeight/2-_slotoffsetz]){
            rotate([0,0,180])
            rotate([90,0,0]){
                for(j=[-1:2:1]){
                    for(i=[-1:2:1]){
                        translate([i*_modx*0.5, j*_modx*0.5, 0])socketBolt(3,length=20);
                    }//i
                }//j
            }
        }
       }
   }
}

module xidlemount(){
    translate([_idleBlockLength/2 ,0,  _fullHeight/2])
       roundedBox([_idleBlockLength   ,_blockWidth,_fullHeight],3,true);
}
module xidlesupports(){
    color(_col)
    intersection(){
        translate([_idleBlockLength-_blockWidth/2     ,0,  _fullHeight/2])
            roundedBox([_blockWidth   ,_blockWidth,_fullHeight],3,true);
        for ( i = [-14:0]){
            for ( j = [0:2]){
                translate([_idleBlockLength+i+0.5,-_blockWidth/2+j*(2+(_blockWidth-4)/3)+2,_fullHeight/2])cube([0.5,(_blockWidth-4)/3,28],center=true);
            }//j
        }//i
    }// intersection
}
module xmotorsupports(){
   translate([_motorBlockLength-42,-_blockWidth/2,_fullHeight/2-_slotoffsetz-_modx/2-0.5]){
        supports(c=[18,(_blockWidth-4)/3,_modx+1],dir=0);
        translate([0,  ((_blockWidth-4)/3+2),0])
            supports(c=[18,(_blockWidth-4)/3,_modx+1],dir=0);
        translate([0,2*((_blockWidth-4)/3+2),0])
            supports(c=[18,(_blockWidth-4)/3,_modx+1],dir=0);
    }
}
module idletensioner(d=12,h=8){
    translate([0,0,0]){
        tensionblock(d=d,h=h);
        if (_showBolts==1){
            color("Silver")translate([ 2,0,_slotoffsetz]) rotate([90,0,0])
                GT2Nidle(20, d=18, h=8.5);
        }
    }
}
module tensionblock(d=12,h=8){

    dx=10.0;
    dy=19.1;
    dz=_slotheight-1;
    dxhole = 2;

    difference(){
      
      
        union() {

            difference(){
                // main structure
               color("Red") union(){

                    translate([-14.5,0,0])tensionarm();

                    intersection(){
                       translate([ -2,0,0]) roundedBox([dx+9,dy,dz],3,true);
                       translate([ 4,0,0]) bowedBox([_modx-3,dy+2, _slotheight],f=0.3);

                    } // intersection

                } // union
                // slot
                translate([5,0,0]) cube([dx+5,h+1,dz+1],center=true);
                // wheel cylinder space
                translate([dxhole,(h+1)/2,_slotoffsetz])rotate([90,0,0]) 
                     cylinder(d=d+1,h=h+1);
            }// diff

            // mount points
            hull(){
                translate([dxhole,   (h+1)/2,_slotoffsetz])rotate([ 90,0,0])cylinder(d=7,h=0.5);
                translate([dxhole, (h+1)/2+1,_slotoffsetz])rotate([ 90,0,0])cylinder(d=9,h=0.5);
            }
            hull(){
                translate([dxhole,  -(h+1)/2,_slotoffsetz])rotate([-90,0,0])cylinder(d=7,h=0.5);
                translate([dxhole,-(h+1)/2-1,_slotoffsetz])rotate([-90,0,0])cylinder(d=9,h=0.5);
            }

        }// union

        // axle bolt hole
       translate([dxhole, 11.0,_slotoffsetz])rotate([90,0,0])cylinder(d=6.0,h=2 ,$fn=30);
       translate([dxhole, 9.0,_slotoffsetz])rotate([90,0,0]) socketBolt(3,length=20,tolerance=5*_tol);// cylinder(d=3.3,h=40,$fn=8 );
        // axel nut hole
       translate([dxhole,-8.0,_slotoffsetz])rotate([90,0,0])nut(size=3,tolerance=_tol*2);
 
        }// diff

    if (_showBolts==1){
         // axle bolt hole
     color("silver") translate([dxhole, 9.0,_slotoffsetz])rotate([90,0,0]) socketBolt(3,length=20,tolerance=2*_tol);
      color("silver")  translate([dxhole,-8.0,_slotoffsetz])rotate([90,0,0])nut(size=3,tolerance=_tol*2);
   }
}
module tensionarm(){
    dx=_idleBlockLength-15;
    //echo("IL",_idleBlockLength-15.0,"dx",dx);
    dy=_slotwidth-0.5;
    dz=_slotheight-1;
    difference(){
        union(){
            hull(){
                translate([-10,0,_slotoffsetz])cube([dx  ,dy  ,dz],center=true);
                translate([-10,0,_slotoffsetz])cube([dx+1,dy-1,dz-1],center=true);
            }// hull
        }// union
        translate([-dx/2,0,_slotoffsetz])cube([4  ,dy+2,7.2],center=true); // M4
        translate([3,0,_slotoffsetz])rotate([0,-90,0])rotate([0,0,0])cylinder(d=4.6,h=dx,$fn=6); // M4
    }//diff
}
