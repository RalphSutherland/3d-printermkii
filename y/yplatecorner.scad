use <std/boxes.scad>;
use <std/fasteners.scad>;
use <std/knobs.scad>;
include <../const.scad>;

$fn = 60;

_delta = 5;
_spring_diameter = 8.0; // all mm
_space = 3.0;
_insetx = 22.5; //22.5 std
_insety = 22.5;
_width = 35.0;//mm overall
_thick = 20.0;// overall, not incluing lips which are +2mm

print();

module print(){
  translate([0,_delta,_delta]) rotate([0,-90,0])
     _corner();
 // translate([40,20,2.5])
  //translate([0,0,2.5])
 //    knob(d=40, depth=4, n=24, h=5, hole=4);
}


module _corner(){
    difference(){

        translate([-_delta,-_delta,0])
        union(){
            translate([_width/2,_width/2,0])
               roundedBox([_width,_width,_thick],3,true);
            translate([_insetx+_tol,_insety+_tol,-_thick/2-1]+[_delta,_delta,0])
               cylinder(d=14,h=_thick+2);
        }

        translate([0,0,1-_tol])
            cube([_width,_width,_space+(2*_tol)]);

        translate([_insetx+_tol,_insety+_tol,-16]) {
            boltShaft(size=4, length=40, tolerance=4*_tol);// tight but not std  fit ~4.3 vs 4.5, _tol ~0.1
            translate([0,0,8]+[0,0,16]) // 5mm thick at base, 3mm deep hole with lip
               cylinder(d=_spring_diameter,h=5);
        }

    }
}

module yplateCorner(){
    _corner();
    translate([_insetx+_tol,_insety+_tol,-13.5]) {
        translate([0,0,0]) rotate([180,0,0])
           knob(d = 40, depth=4, n=24, h=5, hole=4);
    }
}
