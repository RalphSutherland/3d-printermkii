use <std/boxes.scad>;
use <std/fasteners.scad>;

_prof     = 30;// 20,30
_wall     = 2;//mm
_tol      = 0.2;//mm

_room = 25;// room behind plate

$fn = 30;

// t= thickness
// d = array of diameters
// shape array, holes : 0 circle, 1 square, 2 2:1 rectangle
// s = array of strings

//panel(thick=3,d=[16,16,8,18], shape=[0,0,0,2], s=["H","O","BT","B"]);
panel(thick=3,d=[16,16,16,16,16], shape=[0,0,0,0,0], s=["X","Y","Z1","Z2","E"]);

module panel( t=5, h=20, d=[16,8],s=["X","Y"], shape=[0,0], p=_prof,flip=0){

  // t = plate thickness, mm
  // h = panel height without mounting, mm
  // p = profile width for mounting, 20, 30, 40 etc
    
       n = len(d);
       difference(){
         translate([(n*p)/2,(p+h)/2,t/2]) roundedBox([(n+1)*p,p+h, t],3,true);
 
        for(i = [0 : n-1]) {
          x=p/2+i*p;
          y=(h/2);
          translate([x,y,-1]){
              if (shape[i]==0) cylinder(d=d[i],h=t+2);
              if (shape[i]==1) translate([0,0,(t+2)/2]) cube([d[i],d[i],t+2],true);
              if (shape[i]==2) translate([0,0,(t+2)/2]) cube([d[i],d[i]/2,t+2],true);
          }
        }
        
        translate([0,p/2+h,-1]) boltHole(size=6,length=t+2);
        translate([n*p,p/2+h,-1]) boltHole(size=6,length=t+2);
        
      }
      
      for(i = [0 : n-1]) {
          x=p/2+i*p;
          y=(p/4)+h;
          translate([x,y,0]){
              linear_extrude(height=t+1){
                 translate([0,flip*p/2,0]) rotate([0,0,flip*180]) 
                      text(s[i],font="Comfortaa:style=Bold",halign="center",size=p/2);
              }
          }
        }

}

module grilley(gridNX=8,hx=8.0,dhx=2.0,dy=40,h=10){
      for(i = [0 : gridNX-1]) {
          translate([i*(hx+dhx),0,-1]) {
             hull(){
                 cylinder(d=hx,h=h);
                 translate([0,dy,0])cylinder(d=hx,h=h);
             }
          }
      }
}
