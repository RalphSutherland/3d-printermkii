//
// Compact Bowden extruder replacement
///
//Author: R Sutherland
//
//License: CC BY-SA 4.0
//License URL: https://creativecommons.org/licenses/by-sa/4.0/
//
use <std/boxes.scad>
use <std/fasteners.scad>
use <std/motors.scad>
use <std/t-slot.scad>
include <../const.scad>
//============================================================
//
// parameters
//
//============================================================

_driveDiameter = 11.0;// mm max diameter of gear teeth
// steel cylnder = 12mm
// brass max micron gear = 11mm - default design
// Mk8 hobbled steel: 9mm outer, 7mm inner;
//

_bheight   = 64.0; // bracket height
_bthick    = 11.0; // bracket thickness
_thick     = 18.0;
_pthick    =  8;
_gap       =  1.0;

_side   = _mod; // nema 17 motor width
_mdiam  =   24; // diameter of motor collar, may vary from motor to motor
_mdepth =  2.6; // height of motor collar ditto

_showmotor = 1;
_showarm   = 1;
//============================================================
//
// begin main program
//
extruderUnder();

//translate ([42.7,-5.5,-11]) rotate([0,-90,0])  
//           arm(); // in printing position, must be printed in this orientation with supports, filament must run along arm


//============================================================
module extruderUnder(size=_prof, l=212, m=6){
// uncomment/comment the parts required
//
    translate([-_bthick+16,1,13] ){
     // bracketbelow();
      translate([-5,-l/2,-13])rotate([0,0,90]) 
        barmountBrace(size=size,l=l,m=m);
     // bracket();
      translate([0,-0.5*(_side+_bthick),0]) plate();
      if (_showarm == 1) translate([0,-0.5*(_side+_bthick),0]) arm();
    }
      //translate ([42.7,-5.5,-11]) rotate([0,-90,0])  arm(); // in printing position, must be printed in this orientation with supports, filament must run along arm
}

//============================================================
module bracketBelow(size=_prof, l=212, m=5){
//============================================================
  translate([-0.5*size + _pthick*2,_bheight/2+0.5*(_side+_bthick),-6]) rotate([180,0,0]) {
    difference(){

     color(_fitColor)  hull(){
         translate([0,(_bheight/2),_gap]) roundedBox([size, _bheight, _bthick],3,true);
         translate([0,(_bheight/2),0.25]) cube([size,size,_bthick],center=true);
        //translate([0,size/2,_gap]) cube([size,size,_bthick],center=true);
      }

      // M5 bolts and heads
      union(){
        translate([0,size/2,-_bthick/2]) cylinder(r=4.6,6.1);
        translate([0,size/2,-_bthick/2]) boltHole(5,length=_bthick+2,tolerance=0.1);
      }

      translate([0,size-(_profile),0]) union(){
        translate([0,size/2,-_bthick/2]) cylinder(r=4.6,6.1);
        translate([0,size/2,-_bthick/2]) boltHole(5,length=_bthick+2,tolerance=0.1);
      }

    }
  }
}


//============================================================

//============================================================
module bracket(){
//============================================================
  translate([-0.5*_profile + _pthick*2,0,0]) rotate([90,0,0]) {
    difference(){

     color(_fitColor)  hull(){
        translate([0,_bheight/2,_gap]) roundedBox([_profile, _bheight, _bthick],3,true);
        translate([0,(_side-_profile)/2+_profile,0.25]) cube([_profile,_side-_profile,_bthick],center=true);
        translate([0,_profile/2,_gap]) cube([_profile,_profile,_bthick],center=true);
      }

      // M5 bolts and heads
      union(){
        translate([0,_profile/2,-_bthick/2]) cylinder(r=4.6,6.1);
        translate([0,_profile/2,-_bthick/2]) boltHole(5,length=_bthick+2,tolerance=0.1);
      }

      translate([0,_bheight-(_profile),0]) union(){
        translate([0,_profile/2,-_bthick/2]) cylinder(r=4.6,6.1);
        translate([0,_profile/2,-_bthick/2]) boltHole(5,length=_bthick+2,tolerance=0.1);
      }

    }
  }
}


//============================================================
module plate(){
  //============================================================
  translate([_bthick-6,0.5*(_side+_bthick),0.5*_side] ) rotate([90,180,90])  {
     if ( _showmotor==1) translate([0,0,-51]) rotate([0,0,90]) stepper_motor(17,length=48);
      difference(){
      color(_fitColor) translate([0,0,2.5]) union(){
          
      translate([0,2,-1.5]) roundedBox([_side+2,_side+6,_pthick],3,true);
      translate([0,0.25*_side,6.5]) roundedBox([_side,0.5*_side,_thick],3,true);
     // #translate([28.1,16.1,6.5]) cube([10,10,_thick],true);  // corner patch hack!
     hull(){ // arm mount point
       translate([(0.5*_side-5.5),(-0.5*_side+5.5),-_pthick/2]) cylinder(r=5.5,h=_pthick);
        translate([(0.5*_side-5.5),(-0.5*_side+5.5),-_pthick/2]) cylinder(r=5.0,h=_pthick+1);
      }
      // spring mounting area
      translate([-13.75,-1,9.0])  cube([14.7,10.0,_thick-5],true);
    }

    translate([-13.75,-2.5,11])  cube([11.1,5.0,10],true); // spring mounting area subtract
    translate([-13.75,-5.5,13])  cube([11.1,5.0,_thick-5],true); // spring mounting area subtracts

// stepper_motor_mount(17,slide_distance=0, mochup=true, tolerance=0.1);
    translate([ 0, 0,-3.1])  cylinder(r2=_mdiam*0.5,r1=1+(_mdiam*0.5),h=_mdepth+0.1); // collar indent

// Motor drive shaft
//Mk8 steel
//#translate([ 0, 0,6.5])  cylinder(d=_driveDiameter,h=11);
//#translate([ 0, 0,6.5])  cylinder(d=_driveDiameter+2,h=4);
//#translate([ 0, 0,13.5])  cylinder(d=_driveDiameter+2,h=4);
//12mm steel
//#translate([ 0, 0,8.5])  cylinder(d=_driveDiameter,h=11);
// 11mm brass
#translate([ 0, 0,6.5])  cylinder(d=_driveDiameter-2,h=11);
#translate([ 0, 0,10.5])  cylinder(d=_driveDiameter,h=7);

// shaft bore
    translate([ 0, 0,-1])  cylinder(d=_driveDiameter+1.5,h=30);

// mounting holes
    translate([ 15.5, 15.5,-4])  cylinder(r=1.725,h=30);
    translate([ 15.5, 15.5,_thick-3])  cylinder(r=3.0,h=4);
    translate([-15.5, 15.5,-4])  cylinder(r=1.725,h=30);
    translate([-15.5, 15.5,_thick-3])  cylinder(r=3.0,h=4);
    translate([-15.5,-15.5,-4])  cylinder(r=1.725,h=30);
    translate([-15.5,-15.5,2])  cylinder(r=3.0,h=3.2);
    translate([ 15.5,-15.5,-4])  cylinder(r=1.725,h=30);

    translate([_driveDiameter/2+0.75,30.0,12]) rotate([90,0,0]) cylinder(r=1.25,h=30); // filament hole
    translate([_driveDiameter/2+0.75,21.5,12]) rotate([90,0,0]) cylinder(r=2.8,h=6); // fitting M6 hole, needs tapping and/or heat setting, make h=7 for a deeper hole

    translate([_side/2,0,5]) {
      hull(){
        scale([1,0.5,1]) {cylinder(r=18.5,h=18);}
        translate([10,0,0])scale([1,0.5,1]) {cylinder(r=18.5,h=18);}
      }
    }

  }

  translate([-13.8,0.1,11]) rotate ([90,0,0]) cylinder(r1=2.5,r2=2.0,h=2); 
  // spring retainer bump
}
}

//============================================================
module arm(){
  //============================================================

  difference(){
      color(_fitColor)   
      translate([17.0,(_side*0.5)+5.5,(0.5*12.5)+_side-12]) rotate([0,90,0]) {
      difference(){

        union() {
          difference(){
            union(){roundedBox([12.5,_side,12],3,true);}
            translate([6.75,13.8,0]) rotate([0,90,0]) cube([9,9,2.5],center=true); // spring retainting area
          }

           translate([5.0,13.8,0]) rotate([0,90,0]) cylinder(r1=2.5,r2=2.0,h=2); // spring retaining bump

          translate([10,-2,0]) {
            difference(){ // curve section
              cube([9,14,12],true);
              translate([4.85,2,-7.5]) {
                hull(){
                  cylinder(r=8.55,h=14);
                  translate([0,8,0])cylinder(r=8.55,h=14);
                }
              }
              translate([-.5,-7.5,-2.25])cube([14,14,4.5]);
            }
          }

         translate([14-6,-0.5*_side+6.25,0]) {
            difference(){
              roundedBox([28,12.5,12],6.25,true);
              translate([-0.5,-7,-2.25])cube([15,14,4.5]);
            }
          }

           //#   translate([16.4,-13.5,-12.25])cylinder(r=1.75,h=24.5);
           #  translate([16.4,-13.5,-2.25])cylinder(r=6.,h=4.5); // roller

        }

        // bolt holes
         translate([16.4,-13.5,-12.25])cylinder(r=1.85,h=24.5); // will need careful heat melt placing of M4 bolt
         translate([-0.1,-_side*0.5+5.6,-16])cylinder(r=1.7,h=24.5);
         translate([-0.1,-_side*0.5+5.6,3])cylinder(r=3.0,h=4.5);

      }
    }

    translate([_bthick-6,0.5*(_side+_bthick),0.5*_side] ) rotate([90,180,90]) {
        translate([_driveDiameter/2+0.75,10,12.0]) rotate([90,0,0]) cylinder(r=1.25,h=40);
    }

  }

}




module barmountBrace(size=_profile, msize=50, l=100, m = 6){

    thick = 6;
    echo ("barmountBrace length:",l);
    color(_fitColor){
        
difference(){
    union(){
    _brace(30);

    // floor
    hull(){
       translate([  size*1.3,-size/2,0])cube([2,size,thick]);
       translate([l-size*1.3,-size/2,0])cube([2,size,thick]);
    }

    // mount
    
    hull(){
       translate([ l/2+msize/2,  0, 0])cylinder(d=size,h=thick*2,$fn=30);
       translate([ l/2-msize/2,  0, 0])cylinder(d=size,h=thick*2,$fn=30);
    }

    
    // ridges
    hull(){
       translate([  size,-size/2, 0])cube([2,(thick+1)/2,thick*2]);
       translate([l-size,-size/2, 0])cube([2,(thick+1)/2,thick*2]);
    }

    hull(){
       translate([  size*1.5,  0, 0])cylinder(d=(thick+1)/2,h=thick*2,$fn=30);
       translate([l-size*1.5,  0, 0])cylinder(d=(thick+1)/2,h=thick*2,$fn=30);
    }

    hull(){
       translate([  size,size/2-(thick+1)/2,0])cube([2,(thick+1)/2,thick*2]);
       translate([l-size,size/2-(thick+1)/2,0])cube([2,(thick+1)/2,thick*2]);
    }

    translate([l,0,0]) {
        rotate([0,0,180])_brace(30);
    }
    
    }

          boltd = (m==5)?(5.50+_tol):((m==6)?6.60+_tol:8.80+_tol);

           translate([l/2, 0,-1]) {
               hull(){
                  cylinder(d=boltd,h=thick+2,$fn=50);
                  translate([-2,0,0])cylinder(d=boltd,h=thick,$fn=50);
               }
               hull(){
                  translate([0 ,0,thick])cylinder(d=boltd+5,h=20,$fn=50);
                  translate([-2,0,thick])cylinder(d=boltd+5,h=20,$fn=50);
               }
           }
       }
    }
}

