//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>
use <std/t-slot.scad>
use <frame/extruder-under.scad>
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

//======================================================================
// main program, comment components as needed
//======================================================================

l = _insideLength + 2*_prof;
w = _insideWidth;
hg = _insideHeight;

translate([0,0,_prof]){
    translate([0,0,0]) frame();
    translate([0,0,0]) feet();
    translate([0,0,0]) sheets();
    //midfoot();
    //barBrace(size=30,l=(-_yoff+w/2-_prof));
    //semibarBrace(size=_prof,l=(_insideLength-_rodspacingx-42)/2);

    }

//======================================================================
// modules
//======================================================================


module baseframe(){
union(){
// sides
translate([-l/2+_h,0,-_h]) rotate([90,0,0])tslotProfile(size=_prof,length=w);
translate([ l/2-_h,0,-_h]) rotate([90,0,0])tslotProfile(size=_prof,length=w);
// ends
translate([0,-w/2+_h-_prof,-_h]) rotate([0,90,0])tslotProfile(size=_prof,length=l);
translate([0, w/2-_h+_prof,-_h]) rotate([0,90,0])tslotProfile(size=_prof,length=l);
//bottom bar
//translate([0,-_yoff-_h,-_h]) rotate([0,90,0])Profile(height=l-2*_prof,size=_prof);
}
}

module topframe(){
union(){
// sides
translate([-l/2+_h,0,-_h]) rotate([90,0,0]) tslotProfile(size=_prof,length=w);
translate([ l/2-_h,0,-_h]) rotate([90,0,0]) tslotProfile(size=_prof,length=w);

// ends
translate([0,-w/2+_h-_prof,-_h]) rotate([0,90,0]) tslotProfile(size=_prof,length=l);
translate([0, w/2-_h+_prof,-_h]) rotate([0,90,0]) tslotProfile(size=_prof,length=l);

//top bar
translate([0,-_yoff-_h,-_h]) rotate([0,90,0]) tslotProfile(size=_prof, length=l-2*_prof);

}
}

module frame(){

baseframe();
translate([0,0,hg+_prof]) {
    topframe();
}

//mid verts
translate([-l/2+_h,-_yoff-_h,hg/2]) rotate([0,0,0])tslotProfile(size=_prof,length=hg);
translate([ l/2-_h,-_yoff-_h,hg/2]) rotate([0,0,0])tslotProfile(size=_prof,length=hg);
//back verts
translate([-l/2+_h,-w/2+_h-_prof,hg/2]) rotate([0,0,0])tslotProfile(size=_prof,length=hg);
translate([ l/2-_h,-w/2+_h-_prof,hg/2]) rotate([0,0,0])tslotProfile(size=_prof,length=hg);
//front verts
translate([-l/2+_h,+w/2-_h+_prof,hg/2]) rotate([0,0,0])tslotProfile(size=_prof,length=hg);
translate([ l/2-_h,+w/2-_h+_prof,hg/2]) rotate([0,0,0])tslotProfile(size=_prof,length=hg);

fittings();

}

module fittings(){

// base corners
translate([-l/2+_prof,-w/2,-_h])  rotate([ 90,0, 90]) cornerBrace(_prof);
translate([-l/2+_prof, w/2,-_h])  rotate([-90,0,-90]) cornerBrace(_prof);
translate([ l/2-_prof,-w/2,-_h])  rotate([-90,0, 90]) cornerBrace(_prof);
translate([ l/2-_prof, w/2,-_h])  rotate([ 90,0,-90]) cornerBrace(_prof);

translate([0,0,hg+_prof]) {
// top corners
translate([-l/2+_prof,-w/2, -_h]) rotate([ 90, 0, 90]) cornerBrace(_prof);
translate([ l/2-_prof,-w/2, -_h]) rotate([-90, 0, 90]) cornerBrace(_prof);

translate([-l/2+_prof, w/2, -_h]) rotate([-90, 0,-90]) cornerBrace(_prof);
translate([ l/2-_prof, w/2, -_h]) rotate([ 90, 0,-90]) cornerBrace(_prof);
}

//translate([-l/2+_h,-w/2, hg])
//rotate([ 180,0,90])
//barmountBrace(size=30,l=(-_yoff+w/2-_prof));
translate([ -l/2+_h,-w/2+((-_yoff+w/2-_prof))/2, hg])
rotate([ 0, 0, 0])
rotate([ 0, 180, 0])
extruderUnder(size=_prof,l=(-_yoff+w/2-_prof));


translate([ l/2-_h,-w/2+((-_yoff+w/2-_prof))/2, hg])
rotate([ 0, 0, 180])
rotate([ 0, 180, 0])
//#barmountBrace(size=30,l=(-_yoff+w/2-_prof));
extruderUnder(size=_prof,l=(-_yoff+w/2-_prof));

color(_fitColor)
translate([-l/2+_h,-w/2, 0])
rotate([  0,0,90])
barBrace(size=30,l=(-_yoff+w/2-_prof));

color(_fitColor)
translate([ l/2-_h,-w/2, 0])
rotate([  0,0,90])
barBrace(size=_prof,l=(-_yoff+w/2-_prof));


// lower side front
translate([-l/2+_h, w/2, 0])  rotate([ 0,0, -90]) cornerBrace(size=_prof);
translate([ l/2-_h, w/2, 0])  rotate([ 0,0, -90]) cornerBrace(size=_prof);

// upper side front
translate([-l/2+_h, w/2, hg])  rotate([ 180,0, -90]) cornerBrace(size=_prof);
translate([ l/2-_h, w/2, hg])  rotate([ 180,0, -90]) cornerBrace(size=_prof);

// upper front
translate([-l/2+_prof, w/2+_h, hg])  rotate([ 180,0, 0]) cornerBrace(size=_prof);
translate([ l/2-_prof, w/2+_h, hg])  rotate([ 180,0, 180]) cornerBrace(size=_prof);

// upper back
translate([-l/2+_prof, -w/2-_h, hg])  rotate([ 180,0, 0]) cornerBrace(size=_prof);//
translate([ l/2-_prof, -w/2-_h, hg])  rotate([ 180,0, 180]) cornerBrace(size=_prof);

/* upper mid
translate([-l/2+_prof, -_yoff-_h, hg])  rotate([ 180,0, 0]) cornerBrace(size=_prof);
translate([ l/2-_prof, -_yoff-_h, hg])  rotate([ 180,0, 180]) cornerBrace(size=_prof);
*/
// braces to control rod sep
     color(_fitColor)
    translate([-(_insideLength/2), -w/2-_h,0]) semibarBrace(size=_prof,l=(_insideLength-_rodspacingx-42)/2);

    color(_fitColor)
    translate([ (_insideLength/2), -w/2-_h,0]) rotate([0,0,180])semibarBrace(size=_prof,l=(_insideLength-_rodspacingx-42)/2);

    color(_fitColor)
    translate([-(_insideLength/2), w/2+_h,0]) semibarBrace(size=_prof,l=(_insideLength-_rodspacingx-42)/2);

     color(_fitColor)
     translate([ (_insideLength/2), w/2+_h,0]) rotate([0,0,180])semibarBrace(size=_prof,l=(_insideLength-_rodspacingx-42)/2);
}

module sheet(c=[10,10,3],inset=5){
    cube(c);
}


module corneredSheet(s=[10,10,3],c=[0,0,0,0],inset=5){
    echo("Sheet: ",s[0]," x ", s[1], "mm");
    union(){
    difference(){
        cube(s);
        d=sqrt(2)*(inset+_tol);
        translate([0,0,s[2]/2]){
           if (c[0]==1) rotate([0,0,45]) cube([d,d,s[2]+_tol*2], center=true);
           if (c[1]==1) translate([s[0],0,0])rotate([0,0,45]) cube([d,d,s[2]+_tol*2], center=true);
           if (c[2]==1) translate([s[0],s[1],0])rotate([0,0,45]) cube([d,d,s[2]+_tol*2], center=true);
           if (c[3]==1) translate([  0,s[1],0])rotate([0,0,45]) cube([d,d,s[2]+_tol*2], center=true);
        }
    }
}
}

module sheets(){
    thick = 3.0; // mm
    corner = 2*_prof+7;

    color(_fitClearColor){

    translate([-l/2,-w/2-_prof,-_prof-thick-_tol*2])
        cube([l,w+_prof*2,3]);
      //  corneredSheet(s=[l,w+_prof*2,3],c=[1,1,1,1],inset=corner);

    //sides
    translate([-l/2-thick-_tol,-w/2-_prof,hg+_prof]) rotate([0,90,0])
        cube([hg+_prof*2,212+_prof*2,3]);
      //  corneredSheet(s=[hg+_prof*2,212+_prof*2,3],c=[0,1,0,0],inset=corner);

    translate([l/2+_tol,-w/2-_prof,hg+_prof]) rotate([0,90,0])
        cube([hg+_prof*2,212+_prof*2,3]);
     //   corneredSheet(s=[hg+_prof*2,212+_prof*2,3],c=[0,1,0,0],inset=corner);

// front sides
    wid=280;
    translate([-l/2-thick-_tol,w/2-wid+_prof,hg+_prof]) rotate([0,90,0])
        cube([hg+_prof*2,wid,3]);
     //   corneredSheet([hg+_prof*2,wid,3],c=[0,0,1,0],inset=corner);

    translate([l/2+_tol,w/2-wid+_prof,hg+_prof]) rotate([0,90,0])
        cube([hg+_prof*2,wid,3]);
     //   corneredSheet([hg+_prof*2,wid,3],c=[0,0,1,0],inset=corner);


// top
    translate([-l/2-thick-_tol,-w/2-_prof,hg+_prof]) rotate([0,0,0])
         cube([l,212+_prof*2,3]);
      //   corneredSheet([l,212+_prof*2,3],inset=corner);

    translate([-l/2-thick-_tol,w/2-wid+_prof,hg+_prof]) rotate([0,0,0])
         cube([l,wid,3]);
      //   corneredSheet([l,wid,3],inset=corner);

    }
}

module midfoot(){

    $fn=90;
    Msize=6;

    color(_zaxisColor) {

            difference(){
            hull(){
                cylinder(r=26,h=_base);
                translate([0,0,_base])cylinder(r=33.5,h=_prof+7);
            }
            translate([-_prof+_h,-40,_base])cube([2*_prof,80,60]);
            translate([+_h,-40,-1])cube([2*_prof,80,60]);

            translate([0,0,-1]) {
                boltHole(size=Msize,length=2*_prof,tolerance = _tol);
                cylinder(d1=16,d2=13,h=_base-6);
             }

      }

            translate([ 0,-20,_base])cylinder(d=8,h=2);
            translate([ 0,20,_base])cylinder(d=8,h=2);

            translate([ -_prof/2,-20,_base+_prof/2])rotate([0,90,0])cylinder(d=8,h=2);
            translate([ -_prof/2, 20,_base+_prof/2])rotate([0,90,0])cylinder(d=8,h=2);

  }
}

module foot(){
    $fn=90;
    Msize=6;
    color(_footColor)
            difference(){
            intersection(){
            hull(){
                                      cylinder(r=40,h=_base);
                translate([0,0,_base])cylinder(r=_prof,h=1*_prof);
            }
            translate([0,0,_base-5])sphere(d=2.8*_prof,$fn=90);
            }

            translate([-_prof+_h,-2*_prof+_h,_base])cube([1.9*_prof,1.9*_prof,1.01*_prof]);
            // base bolts
            translate([0,0,-1]) {
                boltHole(size=Msize,length=2*_prof,tolerance = _tol);
                washerHole(size=Msize,length=_base-6,tolerance = _tol);
             }
            translate([_prof,0,-1]) {
                boltHole(size=Msize,length=2*_prof,tolerance = _tol);
                washerHole(size=Msize,length=_base-6,tolerance = _tol);
             }
            translate([0,-_prof,-1]) {
                boltHole(size=Msize,length=2*_prof,tolerance = _tol);
                washerHole(size=Msize,length=_base-6,tolerance = _tol);
            }
            // side bolts
             translate([0,11+_prof,_base+3*_prof/2]) rotate([90,0,0]){
                boltHole(size=Msize,length=2*_prof,tolerance = _tol);
                washerHole(size=Msize,length=_base-6,tolerance = _tol);
             }
             translate([-(11+_prof),0,_base+3*_prof/2]) rotate([0,90,0]){
                boltHole(size=Msize,length=2*_prof,tolerance = _tol);
                washerHole(size=Msize,length=_base-6,tolerance = _tol);
             }
            }
       translate([20,0,_base])cylinder(d=8,h=2);
       translate([0,-20,_base])cylinder(d=8,h=2);
       translate([_prof/2+5,_prof/2,_base+_prof/2])rotate([90,0,0])cylinder(d=8,h=2);
       translate([-_prof/2,-_prof/2-5,_base+_prof/2])rotate([0,90,0])cylinder(d=8,h=2);

}

module feet(standoffy = 30.0){

    translate([ 0,+w/2-_h+_prof,-_prof-_base]) {
        translate([-l/2+_h,0,0]) foot();
        translate([ l/2-_h,0,0]) rotate([0,0,-90])foot();
    }

    translate([ 0,-w/2+_h-_prof,-_prof-_base]) {
        translate([-l/2+_h,0,0]) rotate([0,0,90])foot();
        translate([ l/2-_h,0,0]) rotate([0,0,180])foot();
    }

    translate([ 0,standoffy-_yoff,-_prof-_base]) {
        translate([-l/2+_h,0,0]) rotate([0,0,180])midfoot();
        translate([ l/2-_h,0,0]) rotate([0,0,  0])midfoot();
    }
/*
    translate([0,0,hg]){
    translate([ 0,+w/2-_h+_prof,+_prof+_base]) {
        translate([-l/2+_h,0,0]) rotate([180,0,-90])foot();
        translate([ l/2-_h,0,0]) rotate([180,0,180])foot();
    }

    translate([ 0,-w/2+_h-_prof,+_prof+_base]) {
        translate([-l/2+_h,0,0]) rotate([180,0,0])foot();
       translate([ l/2-_h,0,0]) rotate([180,0,90])foot();
    }
}
*/

}


module liteframe(){

baseframe();

//mid verts
translate([-l/2-_h,-_yoff-_h,hg/2-40]) rotate([0,0,0])Profile(height=hg,size=_prof);
translate([ l/2+_h,-_yoff-_h,hg/2-40]) rotate([0,0,0])Profile(height=hg,size=_prof);

//top bar
translate([0,-_yoff-_h,hg+_h-40]) rotate([0,90,0])Profile(height=_topWidth+100,size=_prof);

}
