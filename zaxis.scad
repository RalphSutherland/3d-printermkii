//======================================================================
// Uses
//======================================================================
use <std/motors.scad>
use <z/zmount.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

l = _insideLength + 2*_prof;
w = _insideWidth;
hg = _insideHeight;

_showMotors = 1;
//======================================================================
// main program, comment components as needed
//======================================================================

//zplateleft();
//zplateright();
zaxis(30);

//======================================================================
// modules
//======================================================================

module zaxis(standoffy = _yoffxz){

    _zstandoffy = standoffy;

    translate([-l/2,-_yoff, 0]) {
        translate([ _prof, (standoffy-(_moc+9)), 3]) mirror([1,0,0])zplate(_prof,h=5);
        translate([ _prof, (standoffy-(_moc+9)), 0]) mirror([1,0,0])zplatebase(_prof,h=3);
        if (_showMotors==1) translate([ -_moc-1  ,_zstandoffy,-48]) rotate([0,0,90]) stepper_motor(17);
        color("SlateGrey") translate([ -_moc-1  ,_zstandoffy,22]) cylinder(d=9,h=_zrods);
        color("silver")    translate([ -_moc-1  ,_zstandoffy,22-25/2]) cylinder(d=22,h=25);
        color("silver")    translate([ -_moc+_rodoffsetx-1,_zstandoffy,0]) cylinder(d=_zdiam,h=_zrods);
        translate([ _prof, (standoffy-(_moc+9)), _insideHeight]) mirror([1,0,0])zplatetop(_prof);
    }

    translate([ l/2, -_yoff, 0]) {
        translate([ -_prof,(standoffy-(_moc+9)),3])zplate(prof=_prof,h=5);
        translate([ -_prof,(standoffy-(_moc+9)),0])zplatebase(prof=_prof,h=3);
        if (_showMotors==1) translate([  _moc+1  ,_zstandoffy,-48]) rotate([0,0,90]) stepper_motor(17);
        color("SlateGrey") translate([  _moc+1  ,_zstandoffy,22]) cylinder(d=9,h=_zrods);
        color("silver")    translate([ _moc+1  ,_zstandoffy,22-25/2]) cylinder(d=22,h=25);
        color("silver")    translate([  _moc-_rodoffsetx+1,_zstandoffy, 0]) cylinder(d=_zdiam,h=_zrods);
        translate([ -_prof, (standoffy-(_moc+9)), _insideHeight]) zplatetop(_prof);
    }
}
